﻿use [labor_sql];
go

--------------------------------------            1               ---------------------------------
/*БД «Аеропорт». Пронумерувати всі рейси з таблиці Trip в порядку зростання їх номерів. Виконати сортування по {id_comp, trip_no}. */

SELECT ROW_NUMBER() OVER (ORDER BY [id_comp]) AS [num], [trip_no],[id_comp] FROM [trip]
GO

--------------------------------------            2               ---------------------------------
/*БД «Аеропорт». Пронумерувати рейси кожної компанії окремо в порядку зростання номерів рейсів.*/

SELECT ROW_NUMBER() OVER (PARTITION BY [id_comp] ORDER BY [id_comp]) AS [num], [trip_no],[id_comp] FROM [trip]
GO

--------------------------------------            3               ---------------------------------
/*БД «Комп. фірма». Виберіть найдешевші моделі принтерів в кожній категорії. */

SELECT [model],[color],[type],[price] FROM (SELECT *, MIN([price]) OVER (PARTITION BY [type]) AS [min_price] FROM [printer]) AS q WHERE [price] = [min_price]
GO

--------------------------------------            4               ---------------------------------
/*БД «Комп. фірма». Знайти виробників, які виробляють більше 2-х моделей PC*/

SELECT DISTINCT [maker] FROM (SELECT *, COUNT([model]) OVER (PARTITION BY [maker]) AS [num] FROM [product] WHERE [type] = 'pc') AS q WHERE [num] > 2 
GO

--------------------------------------            5               ---------------------------------
/*БД «Комп. фірма». Знайти друге за величиною значення ціни в таблиці PC*/

SELECT DISTINCT [price] FROM (SELECT *, DENSE_RANK() OVER ( ORDER BY [price] DESC) AS [num] FROM [pc]) AS q WHERE [num] = 2
GO

--------------------------------------            6               ---------------------------------
/*БД «Аеропорт». Розподілити пасажирів по 3-х групах порівну. Групи заповнюються в порядку зростання прізвища (друге слово в стовпці Name).*/

;WITH [upper_case] AS
(SELECT [id_psg], CAST(RTRIM([name])AS VARCHAR(30)) AS [name], n=CAST(RTRIM([name])AS VARCHAR(30)), m=1  FROM [passenger] 
UNION ALL
SELECT [id_psg]+100, [name], CAST(CONCAT(RIGHT([name],m+1), [name])AS VARCHAR(30)), m+1 FROM [upper_case] WHERE ASCII(LEFT(RIGHT([name],m),1)) > ASCII('Z') )
SELECT [name], NTILE(3) OVER (ORDER BY n) [group_numder]
  FROM [upper_case] WHERE ASCII(LEFT(n,1)) <= ASCII('Z') AND [id_psg] > 100 ORDER BY n
GO

--------------------------------------            7               ---------------------------------
/*БД «Комп. фірма». Для таблиці PC виведіть інформацію, необхідну для реалізації пагінації (розбивки на сторінки). 
Вихідні дані повинні бути посортовані по стовпцю price за спаданням. Кожна сторінка повинна містити по 3 стрічки.
 Вивід має бути як на рисунку нижче. */

SELECT *, 
		ROW_NUMBER() OVER (ORDER BY [price] DESC) AS [id],
		(SELECT COUNT (*) FROM [pc]) AS [row_total],
		NTILE((SELECT COUNT (*) FROM [pc])/3 + 1) OVER (ORDER BY [price] DESC) AS [page_num],
		(SELECT COUNT(*) FROM [pc])/3 + 1 AS [page_total]
FROM [pc] ORDER BY [price] DESC
GO

--------------------------------------            8               ---------------------------------
/*БД «Фірма прий. вторсировини». Знайти максимальну суму прийому/видачі серед усіх 4-х таблиць бази даних, 
а також тип операції, дату і пункт прийому, коли і де вона була зафіксована.*/

SELECT [max_sum],[type],[point],[date] FROM(
	SELECT MAX([money]) OVER() AS [max_sum], * FROM
	(
		SELECT 'inc' AS [type],[point],[date],[inc] AS [money] FROM [income]
			UNION ALL
		SELECT 'inc' AS [type],[point],[date],[inc] AS [money] FROM [income_o]
			UNION ALL
		SELECT 'out' AS [type],[point],[date],[out] AS [money] FROM [outcome]
			UNION ALL
		SELECT 'out' AS [type],[point],[date],[out] AS [money] FROM [outcome_o]
	) AS q 
) AS z WHERE [max_sum] = [money]
GO

--------------------------------------            9               ---------------------------------
/*БД «Комп. фірма». Для кожного ПК з таблиці PC знайти різницю між його ціною і середньою ціною на моделі з таким же значенням швидкості ЦП, 
а також між його ціною та загальною середньою ціною по таблиці.*/

SELECT *, [price] - AVG([price]) OVER (PARTITION BY [speed]) AS [df_local_price],
			[price] - AVG([price]) OVER () AS [df_total_price],
			AVG([price]) OVER () AS [total_price]
FROM [pc] ORDER BY [speed]
GO