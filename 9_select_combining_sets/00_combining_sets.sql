﻿use [labor_sql];
go

--------------------------------------            1               ---------------------------------
/*1. Написати довільний запит з одним СТЕ .*/

;WITH [q1] AS
(SELECT [name],[launched],[classes].*, n=0 FROM [ships] JOIN [classes] ON [ships].[class] = [classes].[class]
 UNION ALL
SELECT [name],[launched],[class],[type],[country],[numGuns],[bore],[displacement], n+1 FROM [q1] WHERE [numGuns] = 8 AND n<1)
SELECT * FROM [q1]

--------------------------------------            2               ---------------------------------
/*Написати довільний запит з двома СТЕ (в одному є звертання до іншого)*/

;WITH [q1] AS
(SELECT [name],[launched],[classes].*, n=0, m=0 FROM [ships] JOIN [classes] ON [ships].[class] = [classes].[class]
UNION ALL
SELECT [name],[launched],[class],[type],[country],[numGuns],[bore],[displacement], n+1,m+1 FROM [q1] WHERE [numGuns] = 8 AND m<1),
[q2] AS
(SELECT  [name],[launched],[class],[type],[country],[numGuns],[bore],[displacement], n,m=0 FROM [q1] 
UNION ALL
SELECT [name],[launched],[class],[type],[country],[numGuns],[bore],[displacement], n+1,m+1 FROM [q2] WHERE [bore] = 15 AND m<1),
[q3] AS
(SELECT [name],[launched],[class],[type],[country],[numGuns],[bore],[displacement], n,m=0 FROM [q2] 
UNION ALL
SELECT [name],[launched],[class],[type],[country],[numGuns],[bore],[displacement], n+1,m+1 FROM [q3] WHERE [displacement] = 32000  AND m<1),
[q4] AS
(SELECT [name],[launched],[class],[type],[country],[numGuns],[bore],[displacement], n,m=0 FROM [q3] 
UNION ALL
SELECT [name],[launched],[class],[type],[country],[numGuns],[bore],[displacement], n+1,m+1 FROM [q4] WHERE [type] = 'bb' AND m<1),
[q5] AS
(SELECT [name],[launched],[class],[type],[country],[numGuns],[bore],[displacement], n,m=0 FROM [q4] 
UNION ALL
SELECT [name],[launched],[class],[type],[country],[numGuns],[bore],[displacement], n+1,m+1 FROM [q5] WHERE [country] = 'USA' AND m<1),
[q6] AS
(SELECT [name],[launched],[class],[type],[country],[numGuns],[bore],[displacement], n,m=0 FROM [q5] 
UNION ALL
SELECT [name],[launched],[class],[type],[country],[numGuns],[bore],[displacement], n+1,m+1 FROM [q6] WHERE [launched] = 1915 AND m<1),
[q7] AS
(SELECT [name],[launched],[class],[type],[country],[numGuns],[bore],[displacement], n,m=0 FROM [q6] 
UNION ALL
SELECT [name],[launched],[class],[type],[country],[numGuns],[bore],[displacement], n+1,m+1 FROM [q7] WHERE [class] = 'Kon' AND m<1)
SELECT * FROM [q7] WHERE n >= 4
GO

--------------------------------------            3               ---------------------------------
/*Написати запит (БД «Geography») котрий вертає регіони першого рівня (результат нижче, колонки можуть
називатися інакше)*/

SELECT q2.* FROM [geography] AS q1 JOIN [geography] AS q2 ON q1.[id] = q2.[region_id] WHERE q2.[region_id] = 1
GO

--------------------------------------            4               ---------------------------------
/*Написати запит (БД «Geography») який вертає дерево для конкретного регіону (наприклад, Івано-Франківськ)
Результат має виглядати наступним чином (колонки можуть називатися інакше)*/
SELECT * FROM [geography]

;WITH [region_tree] AS
(SELECT q2.* FROM [geography] AS q1 JOIN [geography] AS q2 ON q1.[id] = q2.[region_id] WHERE q2.[id] = CAST(RAND()*5 AS INT)+2
UNION ALL 
SELECT [geography].* FROM [geography] JOIN [region_tree] ON [region_tree].[id] = [geography].[region_id]) 
SELECT * FROM [region_tree]
GO

--------------------------------------            5               ---------------------------------
/*Виведіть одним запитом список натуральних чисел від 1 до  10 000*/

;WITH [natural_100] AS
(SELECT [num] = 1
	UNION ALL
SELECT [num]+1 FROM [natural_100] WHERE [num]<100
	),

[natural_100x100] AS
(SELECT *, [mult] = 0 FROM [natural_100]
	UNION ALL
SELECT [num] + 100, [mult]+1 FROM [natural_100x100] WHERE [mult]<99)

SELECT [num] FROM [natural_100x100] ORDER BY [num]
GO

--------------------------------------            6               ---------------------------------
/*Виведіть одним запитом список натуральних чисел від 1 до  100 000. */

;WITH [natural_100] AS
(SELECT [num] = 1
	UNION ALL
SELECT [num]+1 FROM [natural_100] WHERE [num]<100
	),

[natural_100x100] AS
(SELECT *, [mult] = 0 FROM [natural_100]
	UNION ALL
SELECT [num] + 100, [mult]+1 FROM [natural_100x100] WHERE [mult]<99),

[natural_100x100x10] AS
(SELECT [num], [mu] = 0 FROM [natural_100x100]
	UNION ALL
SELECT [num] + 10000, [mu]+1 FROM [natural_100x100x10] WHERE [mu]<9)

SELECT [num] FROM [natural_100x100x10] ORDER BY [num]
GO
--------------------------------------            7              ---------------------------------
/*Порахуйте запитом скільки субот і неділь в поточному році. */

;WITH [sat_sun_1] AS
(SELECT DATENAME(WEEKDAY,'2018-01-01') AS [name_day],CAST('2018-01-01' AS DATETIME) AS [day],q=0 
UNION ALL
SELECT DATENAME(WEEKDAY,DATEADD (DAY,q+1,'2018-01-01')) AS [name_day],DATEADD (DAY,q+1 ,'2018-01-01') AS [day],q+1  FROM [sat_sun_1] WHERE q<100
),
[sat_sun_2] AS
(SELECT [name_day],[day],q  FROM [sat_sun_1]
UNION ALL
SELECT DATENAME(WEEKDAY,DATEADD (DAY,q+101,'2018-01-01')) AS [name_day],DATEADD(DAY,q+101,'2018-01-01') AS [day],q+101 FROM [sat_sun_2] WHERE q<264 AND [day] < '2019-01-01'
)
SELECT COUNT([name_day]) FROM [sat_sun_2]  WHERE [name_day] IN ('Saturday','Sunday')
GO

--------------------------------------          8               ---------------------------------
/*БД «Комп. фірма». Знайдіть виробників, що випускають ПК, але не ноутбуки (використати операцію IN). Вивести maker.*/

SELECT DISTINCT [maker] FROM [product]  WHERE [maker] IN (SELECT [maker] FROM [product] WHERE [type] = 'pc') 
	AND [maker] NOT IN (SELECT [maker] FROM [product] WHERE [type] = 'laptop')
GO

--------------------------------------            9               ---------------------------------
/*БД «Комп. фірма». Знайдіть виробників, що випускають ПК, але не ноутбуки (використати ключове слово ALL). Вивести maker.*/

SELECT DISTINCT [maker] FROM (SELECT [maker] FROM [product] WHERE [type] = 'pc') AS q
	WHERE [maker] <> ALL (SELECT [maker] FROM [product] WHERE [type] = 'laptop')
GO

--------------------------------------            10               ---------------------------------
/*БД «Комп. фірма». Знайдіть виробників, що випускають ПК, але не ноутбуки (використати ключове слово ANY). Вивести maker.*/

SELECT DISTINCT [maker] FROM [product]
	WHERE [maker] = ANY(SELECT [maker] FROM [product] WHERE [type] = 'pc')
	AND NOT [maker] = ANY (SELECT [maker] FROM [product] WHERE [type] = 'laptop')
GO

--------------------------------------            11               ---------------------------------
/*БД «Комп. фірма». Знайдіть виробників, що випускають одночасно ПК та ноутбуки (використати операцію IN). Вивести maker.*/

SELECT DISTINCT [maker] FROM [product] 
	WHERE [maker] IN (SELECT [maker] FROM [product] WHERE [type] = 'pc') 
	AND [maker] IN (SELECT [maker] FROM [product] WHERE [type] = 'laptop')
GO

--------------------------------------            12               ---------------------------------
/*БД «Комп. фірма». Знайдіть виробників, що випускають одночасно ПК та ноутбуки (використати ключове слово ALL). Вивести maker.*/

SELECT DISTINCT [maker] FROM (SELECT [maker] FROM [product] WHERE [type] = 'pc') AS q
	WHERE NOT [maker] <> ALL (SELECT [maker] FROM [product] WHERE [type] = 'laptop')
GO

--------------------------------------            13               ---------------------------------
/*БД «Комп. фірма». Знайдіть виробників, що випускають одночасно ПК та ноутбуки (використати ключове слово ANY). Вивести maker.*/

SELECT DISTINCT [maker] FROM (SELECT [maker] FROM [product] WHERE [type] = 'pc') AS q
	WHERE [maker] = ANY (SELECT [maker] FROM [product] WHERE [type] = 'laptop')
GO

--------------------------------------            14               ---------------------------------
/*БД «Комп. фірма». Знайти тих виробників ПК, усі моделі ПК яких є у наявності в таблиці PC (використовувати
вкладені підзапити та оператори IN, ALL, ANY). Вивести maker*/

SELECT DISTINCT [maker] FROM (SELECT * FROM [product] WHERE [type] = 'pc') AS z
	WHERE [maker] <> ANY (
							SELECT DISTINCT [maker] FROM (SELECT * FROM [product] WHERE [type] = 'pc') AS q
								WHERE [model] <> ALL (SELECT DISTINCT [model] FROM [pc])
						)		
GO

--------------------------------------            15               ---------------------------------
/* БД «Кораблі». Вивести класи усіх кораблів України ('Ukraine'). Якщо у БД немає класів кораблів України, тоді
вивести класи для усіх наявних у БД країн. Вивести: country, class.*/

SELECT [country],[class] FROM [classes] 
	WHERE [country] = ALL (SELECT [country] FROM [classes] WHERE [country] = 'Ukraine')
GO

--------------------------------------            16               ---------------------------------
/*БД «Кораблі». Знайдіть кораблі, «збережені для майбутніх битв», тобто такі, що були виведені з ладу в одній битві
('damaged'), а потім (пізніше у часі) знову брали участь у битвах. Вивести: ship, battle, date.*/

SELECT [ship],[battle],[date] FROM (SELECT * FROM [outcomes] JOIN [battles] ON [outcomes].[battle] = [battles].[name]) AS q1
	WHERE [result] = 'damaged'
	AND EXISTS (SELECT * FROM (SELECT * FROM [outcomes] JOIN [battles] ON [outcomes].[battle] = [battles].[name]) AS q2
				WHERE (q1.[date] < q2.[date]) AND q1.[ship] = q2.[ship])
GO

--------------------------------------            17               ---------------------------------
/*БД «Комп. фірма». Знайти тих виробників ПК, усі моделі ПК яких є у наявності в таблиці PC (використовуючи операцію EXISTS). 
Вивести maker.*/

SELECT DISTINCT [maker] FROM [product] AS q1
WHERE NOT EXISTS 
	(SELECT DISTINCT [model] FROM [product] AS q2 WHERE [type] = 'pc' AND q1.[maker] =q2.[maker]
	EXCEPT
	SELECT DISTINCT [model] FROM [pc])
	AND EXISTS (SELECT* FROM [product] AS q2 WHERE [type] = 'pc' AND q1.[maker] =q2.[maker])
GO

--------------------------------------            18               ---------------------------------
/*БД «Комп. фірма». Знайдіть виробників принтерів, що випускають ПК з найвищою швидкістю процесора. Виведіть: maker.*/

SELECT [maker] FROM [product] WHERE  [model] IN (SELECT [model] FROM [pc] WHERE [speed] IN (SELECT MAX([speed]) FROM [pc]))
	INTERSECT
SELECT [maker] FROM [product] WHERE [type] = 'printer'
GO

--------------------------------------            19               ---------------------------------
/*БД «Кораблі». Знайдіть класи кораблів, у яких хоча б один корабель був затоплений у битвах. Вивести: class. 
(Назви класів кораблів визначати за таблицею Ships, якщо його там немає, тоді порівнювати чи його назва 
не співпадає з назвою класу, тобто він є головним)*/

SELECT 
	CASE 
		WHEN [class] IS NOT NULL
			THEN [class]
			ELSE [ship]
		END AS [class]
 FROM (SELECT [ship] FROM [outcomes] WHERE [result] = 'sunk') AS q1 LEFT JOIN [ships] ON q1.[ship] = [ships].[name]
GO

--------------------------------------            20               ---------------------------------
/*БД «Комп. фірма». Знайдіть принтери, що мають найвищу ціну. Вивести: model, price. */


SELECT [model],[price] FROM [printer] WHERE [price] IN (SELECT MAX([price]) FROM [printer])
GO

--------------------------------------            21               ---------------------------------
/*БД «Комп. фірма». Знайдіть ноутбуки, швидкість яких є меншою за швидкість будь-якого з ПК. Вивести: type, model, speed. */

SELECT [type],[laptop].[model],[speed] FROM [laptop] JOIN [product] ON [laptop].[model] = [product].[model]  WHERE  [speed] < (SELECT MIN([speed]) FROM [pc])
GO

--------------------------------------            22               ---------------------------------
/*22. БД «Комп. фірма». Знайдіть виробників найдешевших кольорових принтерів. Вивести: maker, price. */

SELECT [maker],[price] FROM [printer] JOIN [product] ON [printer].[model] = [product].[model] WHERE  [price] IN (SELECT MIN([price]) FROM [printer])
GO

--------------------------------------            23               ---------------------------------
/*БД «Кораблі». Вкажіть битви, у яких брало участь по крайній мірі два корабля однієї і тієї ж країни 
(Вибір країни здійснювати через таблицю Ships, а назви кораблів для таблиці Outcomes, що відсутні у таблиці
Ships, не брати до уваги). Вивести: назву битви, країну, кількість кораблів.*/

SELECT * FROM (
	SELECT [battle], [country],COUNT([ship]) AS [amount] 
		FROM [outcomes] JOIN [ships] ON [outcomes].[ship] = [ships].[name] JOIN [classes] ON [ships].[class] = [classes].[class]
		GROUP BY [battle], [country]
) AS q WHERE [amount]> 1
GO

--------------------------------------            24               ---------------------------------
/*БД «Комп. фірма». Для таблиці Product отримати підсумковий набір у вигляді таблиці зі стовпцями maker, pc, laptop та printer,
 в якій для кожного виробника необхідно вказати кількість продукції, що ним випускається, 
 тобто наявну загальну кількість продукції у таблицях, відповідно, PC, Laptop та Printer.*/

 SELECT * FROM (
	 SELECT [type],[maker],SUM([amount]) AS [amount_of] FROM(
		 SELECT [maker],[product].[model],[amount],[product].[type] FROM 
			(SELECT [model],COUNT([model]) AS [amount] FROM [printer] GROUP BY [model]) AS q1 
			JOIN [product] ON q1.[model] = [product].[model]
				UNION
		 SELECT [maker],[product].[model],[amount],[product].[type] FROM 
			(SELECT [model],COUNT([model]) AS [amount] FROM [laptop] GROUP BY [model]) AS q2 
			JOIN [product] ON q2.[model] = [product].[model]
 				UNION
		 SELECT [maker],[product].[model],[amount],[product].[type] FROM 
			(SELECT [model],COUNT([model]) AS [amount] FROM [pc] GROUP BY [model]) AS q3 
			JOIN [product] ON q3.[model] = [product].[model]
	 ) AS z GROUP BY [type],[maker]
 ) AS info 
	PIVOT 
(SUM([amount_of]) FOR [type] IN ([printer],[laptop],[pc])) AS pvt_table
GO

--------------------------------------            25               ---------------------------------
/*БД «Комп. фірма». Для таблиці Product отримати підсумковий набір у вигляді таблиці зі стовпцями maker, pc, 
в якій для кожного виробника необхідно вказати, чи виробляє він ('yes'), чи ні ('no') відповідний тип продукції. 
У першому випадку  ('yes') додатково вказати поруч у круглих дужках загальну кількість наявної 
(тобто, що знаходиться у таблиці PC) продукції, наприклад, 'yes(2)'.  */

		 SELECT [maker],
			CASE 
				WHEN SUM([amount]) IS NOT NULL
					THEN CONCAT('yes(',CAST(SUM([amount]) AS VARCHAR(4)),')')
					ELSE 'no' 
				END AS [pc]
		 FROM [product] 
			LEFT JOIN (SELECT [model],COUNT([model]) AS [amount] FROM [pc] GROUP BY [model]) AS q3 
			ON q3.[model] = [product].[model] GROUP BY [maker]
			GO
--------------------------------------            26               ---------------------------------
/*БД «Фірма прий. вторсировини». Приймаючи, що прихід та розхід грошей на кожному пункті прийому фіксується
не частіше одного разу на день (лише таблиці Income_o та Outcome_o), написати запит з такими вихідними
даними: point (пункт), date (дата), inc (прихід), out (розхід).*/

SELECT [income_o].[point] ,[income_o].[date],[out],[inc] FROM [income_o] OUTER APPLY (SELECT * FROM [outcome_o] 
	WHERE [income_o].[date] = [outcome_o].[date] AND [income_o].[point] = [outcome_o].[point]) AS q
		UNION
SELECT [outcome_o].*,[inc] FROM [outcome_o] OUTER APPLY (SELECT * FROM [income_o] 
	WHERE [income_o].[date] = [outcome_o].[date] AND [income_o].[point] = [outcome_o].[point]) AS z
GO

--------------------------------------            27               ---------------------------------
/*БД «Кораблі». Визначити назви усіх кораблів з таблиці Ships, які задовольняють, у крайньому випадку,
комбінації будь-яких чотирьох критеріїв з наступного списку: numGuns=8, bore=15, displacement=32000,
type='bb', country='USA', launched=1915, class='Kongo'.4 Вивести: name, numGuns, bore, displacement, type,
country, launched, class.*/

SELECT [name],[launched],[class],[type],[country],[numGuns],[bore],[displacement] FROM(
	SELECT [name],[launched],[classes].*, 
		CASE 
		WHEN [numGuns] = 8 
			THEN 1
			ELSE 0
		END AS [fit_numGuns],
		CASE 
		WHEN [bore] = 15 
			THEN 1
			ELSE 0
		END AS [fit_bore],
		CASE
		WHEN [displacement] = 32000 
			THEN 1
			ELSE 0
		END AS [fit_displacement],
		CASE
		WHEN [type] = 'bb'  
			THEN 1
			ELSE 0
		END AS [fit_type],
		CASE
		WHEN [country] = 'USA' 
			THEN 1
			ELSE 0
		END AS [fit_country],
		CASE
		WHEN [launched] = 1915
			THEN 1
			ELSE 0
		END AS [fit_launched],
		CASE
		WHEN [ships].[class] = 'Kon'
			THEN 1
			ELSE 0
		END AS [fit_class]
	FROM [ships] JOIN [classes] ON [ships].[class] = [classes].[class]) AS q
WHERE [fit_numGuns]+[fit_bore]+[fit_displacement]+[fit_type]+[fit_country]+[fit_launched]+[fit_class] >= 4
--------------------------------------            28               ---------------------------------
/*БД «Фірма прий. вторсировини». Визначіть лідера за сумою виплат у змаганні між кожною парою пунктів з
однаковими номерами із двох різних таблиць – Outcome та Outcome_o – на кожний день, коли здійснювався
прийом вторинної сировини хоча б на одному з них. Вивести: Номер пункту, дата, текст: – 'once a day', якщо
сума виплат є більшою у фірми зі звітністю один раз на день; – 'more than once a day', якщо – у фірми зі
звітністю декілька разів на день; – 'both', якщо сума виплат є однаковою.*/

SELECT [point],[date],
		CASE 
			WHEN [out] > [sum_per_day]
				THEN 'once a day'
			WHEN [out] < [sum_per_day]
				THEN 'more than once a day'
			ELSE 'both'
		END AS [result]
FROM (SELECT [outcome_o].*,[sum_per_day] FROM [outcome_o] 
			JOIN 
		(SELECT [point],[date],SUM([out]) AS [sum_per_day] FROM [outcome] GROUP BY [date],[point]) AS q
		ON [outcome_o].[date] = q.[date] AND [outcome_o].[point] = q.[point]) AS z
GO

--------------------------------------            29               ---------------------------------
/* БД «Комп. фірма». Знайдіть номера моделей та ціни усіх продуктів (будь-якого типу), 
що випущені виробником 'B'. Вивести: maker, model, type, price.*/

	 SELECT * FROM(
		 SELECT [maker],[product].[model],[product].[type],[price] FROM [printer]
			JOIN [product] ON [printer].[model] = [product].[model]
				UNION
		 SELECT [maker],[product].[model],[product].[type],[price] FROM [laptop] 
			JOIN [product] ON [laptop].[model] = [product].[model]
 				UNION
		 SELECT [maker],[product].[model],[product].[type],[price] FROM [pc]
			JOIN [product] ON [pc].[model] = [product].[model]
	 ) AS z WHERE [maker] = 'B'
	 GO
--------------------------------------            30               ---------------------------------
/*БД «Кораблі». Перерахуйте назви головних кораблів, що є наявними у БД (врахувати також і кораблі з таблиці
Outcomes). Вивести: назва корабля, class.*/

SELECT DISTINCT
	CASE 
		WHEN [class] IS NOT NULL
			THEN [name]
			ELSE [ship]
		END AS [name],
	CASE 
		WHEN [class] IS NOT NULL
			THEN [class]
			ELSE [ship]
		END AS [class]
 FROM [outcomes] FULL JOIN [ships] ON [outcomes].[ship] = [ships].[name]
GO

--------------------------------------            31               ---------------------------------
/*БД «Кораблі». Знайдіть класи, у яких входить лише один корабель з усієї БД (врахувати також кораблі у таблиці
Outcomes, яких немає у таблиці Ships). Вивести: class.*/

SELECT [class] FROM (
	SELECT [class],COUNT([name]) AS [amount] FROM (
		SELECT DISTINCT
			CASE 
				WHEN [class] IS NOT NULL
					THEN [name]
					ELSE [ship]
				END AS [name],
			CASE 
				WHEN [class] IS NOT NULL
					THEN [class]
					ELSE [ship]
				END AS [class]
		 FROM [outcomes] FULL JOIN [ships] ON [outcomes].[ship] = [ships].[name]
	) AS q GROUP BY [class]
) AS q2 WHERE [amount] = 1
GO

--------------------------------------            32               ---------------------------------
/*БД «Кораблі». Знайдіть назви усіх кораблів з БД, про які можна однозначно сказати, що вони були спущені на
воду до 1942 р. Вивести: назву кораблів.*/

	SELECT [name],[launched] FROM [ships] WHERE [launched] < 1942
		UNION
	SELECT [ship] AS [name],CAST(YEAR([date]) AS INT) AS [launched] FROM [battles] JOIN [outcomes] ON [battles].[name] = [outcomes].[battle]
		LEFT JOIN [ships] ON [outcomes].[ship] = [ships].[name] WHERE  [ships].[name] IS NULL AND [launched] < 1942
GO