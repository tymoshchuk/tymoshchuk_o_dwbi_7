﻿USE [O_Tymoshchuk_pharmacy]
GO

DECLARE @j SMALLINT = 1
DECLARE @k SMALLINT = 1
DECLARE @number_of_columns int
DECLARE @type_table TABLE ([id] INT, [type] VARCHAR(15))
INSERT INTO @type_table
VALUES 
		(1,'INT'),
		(2,'CHAR(5)'),
		(3,'DATE'),
		(4,'BIT'),
		(5,'FLOAT')
	
DECLARE @employee_name VARCHAR(30)
DECLARE @SQLString NVARCHAR(MAX)	
DECLARE cursor11 CURSOR
FOR SELECT [name] FROM [employee]
OPEN cursor11
FETCH NEXT FROM cursor11 INTO @employee_name
WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @SQLString = 'CREATE TABLE ' + @employee_name +' (id INT)'
		EXECUTE (@SQLString)
			SET @number_of_columns = CAST(RAND()*8 AS INT)+1
				WHILE @j <= @number_of_columns
					BEGIN
						DECLARE @column_name VARCHAR(4) = ''
						WHILE @k <= 4
							BEGIN
								SET @column_name = @column_name + CHAR(CAST(RAND()*25 AS INT)+97)
								SET @k = @k+1
							END
						DECLARE @column_type VARCHAR(7) = (SELECT [type] FROM @type_table WHERE [id]=CAST(RAND()*5 AS INT)+1)
						SET @SQLString = 'ALTER TABLE ' + @employee_name + ' ADD ' + @column_name + ' ' + @column_type
						EXECUTE (@SQLString)
						SET @j = @j + 1
						SET @k = 1
						SET @column_name = ''
					END
				SET @j = 1
		FETCH NEXT FROM cursor11 INTO @employee_name
	END
CLOSE cursor11
DEALLOCATE cursor11

