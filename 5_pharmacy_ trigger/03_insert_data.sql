USE [O_Tymoshchuk_pharmacy]
GO

INSERT INTO [employee]
			([name],
			[middle_name],
			[surname],
			[identify_number],
			[passport],
			[experience],
			[birthday],
			[post],
			[pharmacy_id]
			)
VALUES
			('Alfred','Alfredovych','Alfredenko','1234567891','AA123456',12.3,'1969-01-01','superviser',1),
			('Alfons','Alfonsovych','Alfonsenko','1234567892','AA123457',2.3,'1989-01-01','manager',1),
			('Albert','Albertovych','Albertenko','1234567893','AA123458',2.3,'1989-01-01','manager',2)
GO

INSERT INTO [post]
			([post]
			)
VALUES
			('superviser'),
			('manager')
GO

INSERT INTO [pharmacy]
			([name],
			[building_number],
			[www],
			[work_time],
			[saturday],
			[sunday],
			[street]
			)
VALUES
			('Romashka','12a','www.romashka.com','08:00:00',1,0,'Dovga'),
			('Lopuh','2','www.lopuh.com','08:00:00',1,0,'Korotka'),
			('Lopuh','2','www.lopuh.com','08:00:00',1,0,'Korotka')
GO

INSERT INTO [street]
			([street]
			)
VALUES
			('Dovga'),
			('Korotka')
GO

INSERT INTO [medicine]
			([name],
			[ministry_code],
			[recipe],
			[narcotic],
			[psychotropic]
			)
VALUES
			('Mildronat','AA-111-11',1,0,0),
			('Meldoniy','BB-222-22',1,0,0)
GO

INSERT INTO [pharmacy_medicine]
			([pharmacy_id],
			[medicine_id]
			)
VALUES
			(1,1),
			(2,1)
GO

INSERT INTO [zone]
			([name]
			)
VALUES
			('heart'),
			('kidney')
GO

INSERT INTO [medicine_zone]
			([medicine_id],
			[zone_id]
			)
VALUES
			(1,1),
			(1,2),
			(2,1)
GO