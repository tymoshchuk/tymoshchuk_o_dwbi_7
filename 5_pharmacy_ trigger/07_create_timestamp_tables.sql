USE [O_Tymoshchuk_pharmacy]
GO

DECLARE @id INT
DECLARE @choice SMALLINT
DECLARE @SQLString NVARCHAR(MAX)
DECLARE @full_list_columns VARCHAR(max) = '(
[id] INT,
[name] CHAR(30) NOT NULL,
[middle_name] VARCHAR(30) NULL,
[surname] VARCHAR(30) NOT NULL,
[identify_number] CHAR(10) NULL,
[passport] CHAR(10) NULL,
[experience] DECIMAL(10,1) NULL,
[birthday] DATE NULL,
[post] VARCHAR(10) NOT NULL,
[pharmacy_id] INT NULL
)'
DECLARE @time_name_1 VARCHAR(30) = REPLACE(REPLACE(CAST(CURRENT_TIMESTAMP AS VARCHAR(30)), ' ', '_'), ':','_')
SET @SQLString = 'CREATE TABLE '+ @time_name_1 + ' ' + @full_list_columns
EXECUTE (@SQLString)
SET @SQLString = 'SELECT * FROM ' + @time_name_1
EXECUTE (@SQLString)

WAITFOR DELAY '00:01:00'

DECLARE @time_name_2 VARCHAR(30) = REPLACE(REPLACE(CAST(CURRENT_TIMESTAMP AS VARCHAR(30)), ' ', '_'), ':','_')
SET @SQLString = 'CREATE TABLE '+ @time_name_2+ ' ' + @full_list_columns
EXECUTE (@SQLString)
SET @SQLString = 'SELECT * FROM ' + @time_name_2
EXECUTE (@SQLString)

DECLARE cursor22 CURSOR
FOR SELECT [id] FROM [employee]
OPEN cursor22
FETCH NEXT FROM cursor22 INTO @id
WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @choice = CAST(RAND()*2 AS INT)+1
		DECLARE @char_id VARCHAR(2)= CAST(@id AS VARCHAR(2))
		IF @choice = 1
		BEGIN
			SET @SQLString = 'INSERT INTO ' + @time_name_1 + ' SELECT * FROM [employee] WHERE id = ' + @char_id
			EXECUTE (@SQLString)
		END
		ELSE 
		BEGIN
			SET @SQLString = 'INSERT INTO ' + @time_name_2 + ' SELECT * FROM [employee] WHERE id = ' + @char_id
			EXECUTE (@SQLString)
		END
		FETCH NEXT FROM cursor22 INTO @id
	END 
CLOSE cursor22
DEALLOCATE cursor22

SET @SQLString = 'SELECT * FROM ' + @time_name_1
EXECUTE (@SQLString)
SET @SQLString = 'SELECT * FROM ' + @time_name_2
EXECUTE (@SQLString)