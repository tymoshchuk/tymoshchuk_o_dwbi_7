﻿USE [O_Tymoshchuk_pharmacy]
GO

DROP TABLE IF EXISTS [employee]
DROP TABLE IF EXISTS [post]
DROP TABLE IF EXISTS [pharmacy]
DROP TABLE IF EXISTS [street]
DROP TABLE IF EXISTS [pharmacy_medicine]
DROP TABLE IF EXISTS [medicine]
DROP TABLE IF EXISTS [medicine_zone]
DROP TABLE IF EXISTS [zone]
GO

CREATE TABLE [employee]
(
[id] INT IDENTITY,
[name] CHAR(30) NOT NULL,
[middle_name] VARCHAR(30) NULL,
[surname] VARCHAR(30) NOT NULL,
[identify_number] CHAR(10) NULL,
[passport] CHAR(10) NULL,
[experience] DECIMAL(10,1) NULL,
[birthday] DATE NULL,
[post] VARCHAR(10) NOT NULL,
[pharmacy_id] INT NULL
)
GO

CREATE TABLE [post]
(
[post] VARCHAR(10) NOT NULL
)
GO

CREATE TABLE [pharmacy]
(
[id] INT IDENTITY,
[name] VARCHAR(15) NOT NULL,
[building_number] VARCHAR(10) NULL,
[www] VARCHAR(40) NULL,
[work_time] TIME NULL,
[saturday] BIT NULL,
[sunday] BIT NULL,
[street] VARCHAR(25) NULL
)
GO

CREATE TABLE [street]
(
[street] VARCHAR(25) NOT NULL
)
GO

CREATE TABLE [pharmacy_medicine]
(
[pharmacy_id] INT NOT NULL,
[medicine_id] INT NOT NULL
)
GO

CREATE TABLE [medicine]
(
[id] INT IDENTITY,
[name] VARCHAR(30) NOT NULL,
[ministry_code] CHAR(10) NULL,
[recipe] BIT NULL,
[narcotic] BIT NULL,
[psychotropic] BIT NULL
)
GO

CREATE TABLE [medicine_zone]
(
[medicine_id] INT NOT NULL,
[zone_id] INT NOT NULL
)
GO

CREATE TABLE [zone]
(
[id] INT IDENTITY,
[name] VARCHAR(25) NOT NULL
)
GO
