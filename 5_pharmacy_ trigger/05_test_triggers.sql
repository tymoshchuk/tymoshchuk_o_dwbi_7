﻿USE [O_Tymoshchuk_pharmacy]
GO

SELECT * FROM [employee]
SELECT * FROM [post]
SELECT * FROM [pharmacy]
SELECT * FROM [street]
SELECT * FROM [pharmacy_medicine]
SELECT * FROM [medicine]
SELECT * FROM [medicine_zone]
SELECT * FROM [zone]
GO

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

-----------------------ordinary insert
INSERT INTO [employee]
VALUES
			('Adolf','Adolfovych','Adolfenko','1234567894','AA123459',12.3,'1969-01-01','superviser',1)
GO
SELECT * FROM [employee]
GO

---------------[identify_number] ends with 00 -- not working
INSERT INTO [employee]
VALUES
			('Arthur','Arthurych','Arthurenko','1234567800','AA123459',12.3,'1969-01-01','manager',1)
GO
SELECT * FROM [employee]
GO

----------------------[pharmacy_id] = 5 not exists in [pharmacy] -- replaced with NULL 
INSERT INTO [employee]
VALUES
			('Arthur','Arthurych','Arthurenko','1234567894','AA123459',12.3,'1969-01-01','manager',5)
GO
SELECT * FROM [employee]
GO

-------------------[post] = 'docker' not exists in [post] -- not working 
INSERT INTO [employee]
VALUES
			('Arthur','Arthurych','Arthurenko','1234567894','AA123459',12.3,'1969-01-01','docker',2)
GO
SELECT * FROM [employee]
GO

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------[post] couldn't be updated -- not working
UPDATE [post]
SET [post] = 'docker'
WHERE [post] = 'manager'
GO
SELECT * FROM [post]
GO

----------------------------or deleted -- not working
DELETE FROM [post]
WHERE [post] = 'manager'
GO
SELECT * FROM [post]
GO

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

----------------------ordinary insert 
INSERT INTO [pharmacy]
VALUES
			('Beladonna','3','www.beladonna.com','08:00:00',1,0,'Korotka')
GO
SELECT * FROM [pharmacy]
GO

---------------------[street] = 'Engelsa' is absent in [street] -- not working
INSERT INTO [pharmacy]
VALUES
			('Beladonna','3','www.beladonna.com','08:00:00',1,0,'Engelsa')
GO
SELECT * FROM [pharmacy]
GO

--------------------as well as for updating
UPDATE [pharmacy]
SET [street] = 'Engelsa'
WHERE [id] = 1
GO
SELECT * FROM [pharmacy]
GO

-------------------------- but we could change the [street] until it as in the [street]
UPDATE [pharmacy]
SET [street] = 'Dovga'
WHERE [id] = 3
GO
SELECT * FROM [pharmacy]
GO

----------------after deleting we obtain NULL in the [employee] and deleted fields in the [pharmacy_medicine]
DELETE FROM [pharmacy]
WHERE [id] = 2
GO
SELECT * FROM [pharmacy]
SELECT * FROM [employee]
SELECT * FROM [pharmacy_medicine]
GO

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

------------after changing [street] it also changed in [pharmacy]
UPDATE [street]
SET [street] = 'Kryva'
WHERE [street] = 'Dovga'
GO
SELECT * FROM [street]
GO
SELECT * FROM [pharmacy]
GO

----------------after deleting we obtain NULL in the [pharmacy]

DELETE FROM [street]
WHERE [street] = 'Korotka'
GO
SELECT * FROM [street]
GO
SELECT * FROM [pharmacy]
GO

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------successfull insert of [ministry_code]
INSERT INTO [medicine]
VALUES
			('Mildronat','yu-666-66',1,0,0)
GO
SELECT * FROM [medicine]
GO

---------------not working with 3 letters
INSERT INTO [medicine]
VALUES
			('Mildronat','aaa-111-11',1,0,0)
GO
SELECT * FROM [medicine]
GO

---------------not working letter m
INSERT INTO [medicine]
VALUES
			('Mildronat','ma-111-11',1,0,0)
GO
SELECT * FROM [medicine]
GO
/*
DROP TABLE IF EXISTS [employee22]

CREATE TABLE [employee22]
(
[id] INT IDENTITY,
[name] CHAR(30) NOT NULL,
[middle_name] VARCHAR(30) NULL,
[surname] VARCHAR(30) NOT NULL,
[identify_number] CHAR(10) NULL,
[passport] CHAR(10) NULL,
[experience] DECIMAL(10,1) NULL,
[birthday] DATE NULL,
[post] VARCHAR(10) NOT NULL,
[pharmacy_id] INT NULL
)
GO

ALTER TABLE [employee] DROP COLUMN [id]

INSERT INTO [employee22] SELECT * FROM [employee] WHERE [id] <  4

SELECT * FROM [employee22]*/