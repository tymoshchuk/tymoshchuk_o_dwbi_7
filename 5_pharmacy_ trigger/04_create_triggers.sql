USE [O_Tymoshchuk_pharmacy]
GO

EXEC sp_configure 'show advanced options', 1;
GO
RECONFIGURE ;
GO
EXEC sp_configure 'nested triggers', 1 ;
GO
RECONFIGURE;
GO


DROP TRIGGER IF EXISTS [tr_employee]
DROP TRIGGER IF EXISTS [tr_post]
DROP TRIGGER IF EXISTS [tr_pharmacy]
DROP TRIGGER IF EXISTS [tr_street]
DROP TRIGGER IF EXISTS [tr_pharmacy_medicine]
DROP TRIGGER IF EXISTS [tr_medicine_del]
DROP TRIGGER IF EXISTS [tr_medicine_ins_upd]
DROP TRIGGER IF EXISTS [tr_medicine_zone]
DROP TRIGGER IF EXISTS [tr_zone]
GO

--************************************************************************************************************************

CREATE OR ALTER TRIGGER [tr_employee]
ON [employee]
AFTER INSERT,UPDATE
AS
BEGIN
		IF (SELECT COUNT(INSERTED.[post]) FROM INSERTED JOIN [post] ON INSERTED.[post] = [post].[post]) < (SELECT COUNT(INSERTED.[post]) FROM INSERTED)
		BEGIN
			ROLLBACK
			PRINT 'foreign key restriction'
		END		
	IF (SELECT COUNT(INSERTED.[id]) FROM INSERTED JOIN [pharmacy] ON INSERTED.[pharmacy_id] = [pharmacy].[id]) < (SELECT COUNT(INSERTED.[id]) FROM INSERTED)
		BEGIN
			UPDATE [employee]
			SET [employee].[pharmacy_id] = NULL 
			WHERE [employee].[id] in (SELECT INSERTED.[id] FROM INSERTED)
		END
	IF EXISTS (SELECT [id] FROM INSERTED WHERE INSERTED.[identify_number] LIKE '%00')
		BEGIN
			ROLLBACK
			PRINT 'identify_number couldnt end with 00'
		END
END
GO

--********************************************************************************************************************

CREATE OR ALTER TRIGGER [tr_post]
ON [post]
INSTEAD OF DELETE,UPDATE
AS
	PRINT 'the operation is forbidden'
GO

--*********************************************************************************************************************

CREATE OR ALTER TRIGGER [tr_pharmacy]
ON [pharmacy]
AFTER INSERT,DELETE,UPDATE
AS
BEGIN
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation NCHAR(6)
DECLARE @ins INT = (SELECT COUNT(*)FROM INSERTED)
DECLARE @del INT = (SELECT COUNT(*)FROM DELETED)
SET @operation = 
       CASE
			WHEN @ins>0 AND @del>0 THEN 'update'
			WHEN @ins>0 AND @del=0 THEN 'insert'
			WHEN @ins=0 AND @del>0 THEN 'delete'
	   END
IF @operation in ('insert', 'update')
	BEGIN
		IF (SELECT COUNT(INSERTED.[street]) FROM INSERTED JOIN [street] ON INSERTED.[street] = [street].[street]) < (SELECT COUNT(INSERTED.[street]) FROM INSERTED)
			BEGIN
				ROLLBACK
				PRINT 'foreign key restriction'
			END
	END
IF @operation IN ('delete','update')
	BEGIN
		BEGIN
			UPDATE [employee]
			SET [employee].[pharmacy_id] = NULL 
			WHERE [employee].[pharmacy_id] in (SELECT DELETED.[id] FROM DELETED)
		END
		BEGIN
			DELETE FROM [pharmacy_medicine]
			WHERE [pharmacy_medicine].[pharmacy_id] in (SELECT DELETED.[id] FROM DELETED)
		END		
	END
END
GO

--********************************************************************************************************************

CREATE OR ALTER TRIGGER [tr_street]
ON [street]
AFTER INSERT,DELETE,UPDATE
AS
BEGIN
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation NCHAR(6)
DECLARE @ins INT = (SELECT COUNT(*)FROM INSERTED)
DECLARE @del INT = (SELECT COUNT(*)FROM DELETED)
SET @operation = 
       CASE
			WHEN @ins>0 AND @del>0 THEN 'update'
			WHEN @ins>0 AND @del=0 THEN 'insert'
			WHEN @ins=0 AND @del>0 THEN 'delete'
	   END
IF @operation ='update'
	BEGIN
		UPDATE [pharmacy]
		SET [pharmacy].[street] = INSERTED.[street] 
		FROM INSERTED WHERE [pharmacy].[street] in (SELECT DELETED.[street] FROM DELETED)
	END
IF @operation = 'delete'
	BEGIN
		UPDATE [pharmacy]
		SET [pharmacy].[street] = NULL 
		WHERE [pharmacy].[street] in (SELECT DELETED.[street] FROM DELETED)
	END
END
GO

--*****************************************************************************************************************

CREATE OR ALTER TRIGGER [tr_pharmacy_medicine]
ON [pharmacy_medicine]
AFTER INSERT,UPDATE
AS
BEGIN
		IF ((SELECT COUNT(INSERTED.[pharmacy_id]) FROM INSERTED JOIN [pharmacy] ON INSERTED.[pharmacy_id] = [pharmacy].[id]) < (SELECT COUNT(INSERTED.[pharmacy_id]) FROM INSERTED)) OR
		((SELECT COUNT(INSERTED.[medicine_id]) FROM INSERTED JOIN [medicine] ON INSERTED.[medicine_id] = [medicine].[id]) < (SELECT COUNT(INSERTED.[medicine_id]) FROM INSERTED))
		BEGIN
			ROLLBACK
			PRINT 'foreign key restriction'
		END		
END
GO

--******************************************************************************************************************

CREATE OR ALTER TRIGGER [tr_medicine_del]
ON [medicine]
AFTER DELETE
AS
	BEGIN
		DELETE FROM [pharmacy_medicine]
		WHERE [pharmacy_medicine].[medicine_id] in (SELECT DELETED.[id] FROM DELETED)
		DELETE FROM [medicine_zone]
		WHERE [medicine_zone].[medicine_id] in (SELECT DELETED.[id] FROM DELETED)	
	END
GO

--******************************************************************************************************************

CREATE OR ALTER TRIGGER [tr_medicine_ins_upd]
ON [medicine]
AFTER INSERT, UPDATE
AS
BEGIN
	IF EXISTS (SELECT [id] FROM INSERTED WHERE  INSERTED.[ministry_code] NOT LIKE '[a-Z][a-Z]-[0-9][0-9][0-9]-[0-9][0-9]' OR INSERTED.[ministry_code] LIKE '%m%' OR INSERTED.[ministry_code] LIKE '%p%')
		BEGIN
			ROLLBACK
			PRINT 'ministry_code should looks like: 2 letters (except M or P) - 3 digits - 2 digits'
		END	
END
GO

--*****************************************************************************************************************

CREATE OR ALTER TRIGGER [tr_medicine_zone]
ON [medicine_zone]
AFTER INSERT,UPDATE
AS
BEGIN
		IF ((SELECT COUNT(INSERTED.[zone_id]) FROM INSERTED JOIN [zone] ON INSERTED.[zone_id] = [zone].[id]) < (SELECT COUNT(INSERTED.[zone_id]) FROM INSERTED)) OR
		((SELECT COUNT(INSERTED.[medicine_id]) FROM INSERTED JOIN [medicine] ON INSERTED.[medicine_id] = [medicine].[id]) < (SELECT COUNT(INSERTED.[medicine_id]) FROM INSERTED))
		BEGIN
			ROLLBACK
			PRINT 'foreign key restriction'
		END		
END
GO

--******************************************************************************************************************

CREATE OR ALTER TRIGGER [tr_zone]
ON [zone]
AFTER DELETE
AS
BEGIN
		DELETE FROM [medicine_zone]
		WHERE [medicine_zone].[zone_id] in (SELECT DELETED.[id] FROM DELETED)	
END
GO
