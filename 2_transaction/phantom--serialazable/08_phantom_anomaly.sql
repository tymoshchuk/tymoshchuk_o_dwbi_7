use clinic
go

-- first execute this script, and then 10_insert_new_data_into_person

begin transaction
select * from person
waitfor delay '00:00:04'
select * from person
commit transaction


delete from person
where [name] in ('Alfons','Adolf')
go

select * from person
go
