use clinic
go

-- first execute this script, and then 07_update_person

begin transaction
select * from person
waitfor delay '00:00:04'
select * from person
commit transaction

update person
set [name] = 'Ivan'
where id = 1
go
