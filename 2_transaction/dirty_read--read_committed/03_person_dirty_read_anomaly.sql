use clinic
go

--first, please, execute  02_person_update_with_rollback 

set transaction isolation level read uncommitted
go

begin transaction
select * from person
commit transaction