use clinic
go

--first, please, execute  02_person_update_with_rollback

---    in Microsoft SQlServer 'committed read' is default option, so we can omit this statement
/* 
set transaction isolation level read committed
go*/

begin transaction
select * from person
commit transaction