use clinic
go

begin transaction
update person
set [name] = 'Arthur'
where id = 2
waitfor delay '00:00:04'
rollback transaction

select * from person