﻿USE [labor_sql];
GO

--------------------------------------            1               -----------------------------
/*БД «Комп. фірма».  Знайти виробників ноутбуків. Вивести: maker, type. Вихідні дані впорядкувати за зростанням за стовпцем maker. */

SELECT DISTINCT [maker],[type] FROM [product] WHERE [type] = 'laptop' ORDER BY [maker]
GO

--------------------------------------            2               -----------------------------
/*БД «Комп. фірма».  Знайти номер моделі, об’єм пам’яті та розміри екранів ноутбуків, ціна яких перевищує 1000 дол. 
Вивести: model, ram, screen, price. Вихідні дані впорядкувати за зростанням за стовпцем ram та за спаданням за стовпцем price.*/

SELECT [model],[ram],[screen],[price] FROM [laptop] WHERE [price] > 1000 ORDER BY [ram],[price] DESC
GO

--------------------------------------            3               -----------------------------
/*БД «Комп. фірма».  Знайдіть усі записи таблиці Printer для кольорових принтерів. 
Вихідні дані впорядкувати за спаданням за стовпцем price.*/

SELECT * FROM [printer] WHERE [color] = 'y' ORDER BY [price] DESC
GO

--------------------------------------            4               -----------------------------
/*БД «Комп. фірма». Знайдіть номер моделі, швидкість та розмір диску ПК, 
що мають CD-приводи зі швидкістю '12х' чи '24х' та ціну меншу 600 дол. 
Вивести: model, speed, hd, cd, price. Вихідні дані впорядкувати за спаданням за стовпцем speed*/

SELECT [model],[speed],[hd],[cd],[price] FROM [pc] WHERE [cd] IN ('12x','24x') AND [price] < 600 ORDER BY [speed] DESC
GO

--------------------------------------            5               -----------------------------
/*БД «Кораблі». Перерахувати назви головних кораблів (з таблиці Ships). Вивести: name, class. 
Вихідні дані впорядкувати за зростанням за стовпцем name.*/

SELECT [name],[class] FROM [ships] WHERE [name] = [class] ORDER BY [name]
GO

/*якщо би в умові не було фрази "...(з таблиці Ships)..." то запит мав би виглядати наступним чином:"*/

SELECT * FROM (
	SELECT [name],[class] FROM [ships] WHERE [name] = [class]
		UNION
	SELECT [ship] AS [name],[ship] AS [class] FROM [outcomes] LEFT JOIN [ships] ON [outcomes].[ship] = [ships].[name] 
	WHERE  [ships].[name] IS NULL
) AS q  ORDER BY [name]
GO

--------------------------------------            6               -----------------------------
/*БД «Комп. фірма». Отримати інформацію про комп’ютери, що мають частоту процесора не менше 500 МГц та ціну нижче 800 дол. 
Вихідні дані впорядкувати за спаданням за стовпцем price. */

SELECT * FROM  [product] JOIN [pc] ON [product].[model] = [pc].[model]  WHERE [speed] >= 500 AND [price] < 800 ORDER BY [price] DESC
GO

/*хоча правильним мав би бути такий запит (незважаючи на ідентичний результат):*/
SELECT * FROM (
	SELECT [maker],[pc].[model],[code],[speed],[ram],[hd],[price],[cd],CAST(NULL AS INT) AS [screen] FROM  [product] JOIN [pc] ON [product].[model] = [pc].[model]  
		UNION
	SELECT [maker],[laptop].[model],[code],[speed],[ram],[hd],[price],CAST(NULL AS VARCHAR(10)) AS [cd],[screen] FROM  [product] JOIN [laptop] ON [product].[model] = [laptop].[model] 
) AS q WHERE [speed] >= 500 AND [price] < 800  ORDER BY [price] DESC
GO

--------------------------------------            7               -----------------------------
/*БД «Комп. фірма». Отримати інформацію про всі принтери, які не є матричними та коштують менше 300 дол. 
Вихідні дані впорядкувати за спаданням за стовпцем type*/

SELECT * FROM  [product] JOIN [printer] ON [product].[model] = [printer].[model]  
	WHERE [printer].[type] <> 'Matrix' AND [price] < 300 ORDER BY [printer].[type] DESC
GO

--------------------------------------            8               ------------------------------
/*БД «Комп. фірма». Знайти модель та частоту процесора комп’ютерів, що коштують від 400 до 600 дол.
 Вивести: model, speed. Вихідні дані впорядкувати за зростанням за стовпцем hd.*/

SELECT [model],[speed] FROM  [pc]  WHERE [price] BETWEEN 400 AND 600 ORDER BY [hd] 
GO

--------------------------------------            9               -----------------------------
/*БД «Комп. фірма». Знайдіть номер моделі, швидкість та розмір жорсткого диску для усіх ноутбуків, екран яких не  менше 12 дюймів. 
Вивести: model, speed, hd, price. Вихідні дані впорядкувати за спаданням за стовпцем price.*/

SELECT [model],[speed],[hd],[price] FROM  [laptop]  WHERE [screen] >= 12 ORDER BY [price] DESC
GO

--------------------------------------            10               -----------------------------
/*БД «Комп. фірма». Знайдіть номер моделі, тип та ціну для усіх принтерів, вартість яких менше 300 дол. 
Вивести: model, type, price. Вихідні дані впорядкувати за спаданням за стовпцем type. */

SELECT [model],[type],[price] FROM  [printer]  WHERE  [price] < 300 ORDER BY [type] DESC
GO

--------------------------------------            11               -----------------------------
/*БД «Комп. фірма». Вивести моделі ноутбуків з кількістю RAM рівною 64 Мб.  Вивести: model, ram, price. 
Вихідні дані впорядкувати за зростанням за стовпцем screen. */

SELECT [model],[ram],[price] FROM  [laptop]  WHERE  [ram] = 64 ORDER BY [screen]
GO

--------------------------------------            12               -----------------------------
/*БД «Комп. фірма». Вивести моделі ПК з кількістю RAM більшою за 64 Мб.  Вивести: model, ram, price. 
Вихідні дані впорядкувати за зростанням за стовпцем hd. */

SELECT DISTINCT [model],[ram],[price] FROM  (SELECT TOP(SELECT COUNT(*) FROM [pc]) * FROM [pc] ORDER BY [hd] ) AS q WHERE  [ram] > 64 
GO

--------------------------------------            13               -----------------------------
/*БД «Комп. фірма». Вивести моделі ПК зі швидкістю процесора у межах від 500 до 750 МГц.  Вивести: model, speed, price. 
Вихідні дані впорядкувати за спаданням за стовпцем hd. */

SELECT [model],[speed],[price] FROM  [pc]  WHERE [speed] BETWEEN 500 AND 750 ORDER BY [hd] DESC
GO

--------------------------------------            14               -----------------------------
/* БД «Фірма прий. вторсировини». Вивести інформацію про видачу грошей на суму понад 2000 грн. на пунктах прийому таблиці Outcome_o. 
Вихідні дані впорядкувати за спаданням за стовпцем date.*/

SELECT * FROM [outcome_o] WHERE [out] > 2000 ORDER BY [date] DESC
GO

--------------------------------------            15              -----------------------------
/*БД «Фірма прий. вторсировини». Вивести інформацію про прийом грошей на суму у межах від 5 тис. до 10 тис. грн. на
пунктах прийому таблиці Income_o. Вихідні дані впорядкувати за зростанням за стовпцем inc.*/

SELECT * FROM [income_o] WHERE [inc] BETWEEN 5000 AND 10000 ORDER BY [inc]
GO

--------------------------------------            16               -----------------------------
/*БД «Фірма прий. вторсировини». Вивести інформацію про прийом грошей на пункті прийому №1 таблиці Income. 
Вихідні дані впорядкувати за зростанням за стовпцем inc*/

SELECT * FROM [income] WHERE [point] = 1 ORDER BY [inc]
GO

--------------------------------------            17               -----------------------------
/*БД «Фірма прий. вторсировини». Вивести інформацію про видачу грошей на пункті прийому №2 таблиці Outcome.
Вихідні дані впорядкувати за зростанням за стовпцем out.*/

SELECT * FROM [outcome] WHERE [point] = 2 ORDER BY [out]
GO

--------------------------------------            18               -----------------------------
/*БД «Кораблі». Вивести інформацію про усі класи кораблів для країни 'Japan'. 
Вихідні дані впорядкувати за спаданням за стовпцем type.*/

SELECT * FROM [classes] WHERE [country] = 'Japan' ORDER BY [type] DESC
GO

--------------------------------------            19               -----------------------------
/*БД «Кораблі». Знайти всі кораблі, що були спущені на воду у
термін між 1920 та 1942 роками. Вивести: name, launched.
Вихідні дані впорядкувати за спаданням за стовпцем launched.*/

SELECT [name],[launched] FROM [ships] WHERE [launched] BETWEEN 1920 AND 1942 ORDER BY [launched] DESC
GO 
/*Якщо враховувати битви, які відбулись до кінця 1942 й вибрати додаткові кораблі звідти , 
у нас не буде інформації чи ці кораблі не спущені на воду до 1920 року.
Так що на мою думку наступний запит є неправильним для цієї задачі: */
SELECT * FROM (
	SELECT [name],[launched] FROM [ships] 
		UNION
	SELECT [ship] AS [name],CAST(YEAR([date]) AS INT) AS [launched] FROM [battles] JOIN [outcomes] ON [battles].[name] = [outcomes].[battle]
		LEFT JOIN [ships] ON [outcomes].[ship] = [ships].[name] WHERE  [ships].[name] IS NULL
) AS q  WHERE [launched] BETWEEN 1920 AND 1942 ORDER BY [launched] DESC
GO
  
--------------------------------------            20               -----------------------------
/*БД «Кораблі». Вивести усі кораблі, що брали участь у битві 'Guadalcanal' та не були потопленими. 
Вивести: ship, battle, result. Вихідні дані впорядкувати за спаданням за стовпцем ship*/

SELECT * FROM [outcomes] WHERE [battle] = 'Guadalcanal' AND [result] <> 'sunk' ORDER BY [ship] DESC
GO
--------------------------------------            21               -----------------------------
/*БД «Кораблі». Вивести усі потоплені кораблі. Вивести: ship, battle, result. 
Вихідні дані впорядкувати за спаданням за стовпцем ship.*/

SELECT * FROM [outcomes] WHERE [result] = 'sunk' ORDER BY [ship] DESC
GO

--------------------------------------            22               -----------------------------
/*БД «Кораблі». Вивести назви класів кораблів з водотоннажністю не меншою, аніж 40 тон. 
Вивести: class, displacement. Вихідні дані впорядкувати за зростанням за стовпцем type.*/

SELECT [class],[displacement] FROM [classes] WHERE [displacement] >= 40000 ORDER BY [type]
GO

--------------------------------------            23               -----------------------------
/*БД «Аеропорт». Знайдіть номера усіх рейсів, що бувають у місті 'London'. 
Вивести: trip_no, town_from, town_to. Вихідні дані впорядкувати за зростанням за стовпцем time_out.*/

SELECT [trip_no],[town_from],[town_to] FROM [trip] WHERE [town_from] = 'London' OR [town_to] = 'London' ORDER BY [time_out]
GO

--------------------------------------            24               -----------------------------
/*БД «Аеропорт». Знайдіть номера усіх рейсів, на яких курсує літак 'TU-134'. 
Вивести: trip_no, plane, town_from, town_to. Вихідні дані впорядкувати за спаданням за стовпцем time_out.*/

SELECT [trip_no],[plane],[town_from],[town_to] FROM [trip] WHERE [plane] = 'TU-134' ORDER BY [time_out] DESC
GO

--------------------------------------            25               -----------------------------
/* БД «Аеропорт». Знайдіть номера усіх рейсів, на яких не курсує літак 'IL-86'. 
Вивести: trip_no, plane, town_from, town_to. Вихідні дані впорядкувати за зростанням за стовпцем plane.*/

SELECT [trip_no],[plane],[town_from],[town_to] FROM [trip] WHERE [plane] <> 'IL-86' ORDER BY [plane]
GO

--------------------------------------            26               -----------------------------
/*БД «Аеропорт». Знайдіть номера усіх рейсів, що не бувають у місті 'Rostov'. 
Вивести: trip_no, town_from, town_to. Вихідні дані впорядкувати за зростанням за стовпцем plane.*/

SELECT [trip_no],[town_from],[town_to] FROM [trip] WHERE [town_from] <> 'Rostov' AND [town_to] <> 'Rostov' ORDER BY [plane]
GO

--------------------------------------            27               -----------------------------
/*БД «Комп. фірма». Вивести усі моделі ПК, у номерах яких є хоча б дві одинички.*/

SELECT * FROM  [pc]  WHERE [model] LIKE '%1%1%'
GO

--------------------------------------            28               -----------------------------
/*БД «Фірма прий. вторсировини». З таблиці Outcome вивести усю інформацію за березень місяць.*/

SELECT * FROM [outcome] WHERE MONTH([date]) = 3
GO

--------------------------------------            29               -----------------------------
/*БД «Фірма прий. вторсировини». З таблиці Outcome_o вивести усю інформацію за 14 число будь-якого місяця.*/

SELECT * FROM [outcome_o] WHERE DAY([date]) = 14
GO

--------------------------------------            30               -----------------------------
/*БД «Кораблі». З таблиці Ships вивести назви кораблів, що починаються на 'W' та закінчуються літерою 'n'.*/

SELECT [name] FROM [ships] WHERE [name] LIKE 'W%n'
GO 

--------------------------------------            31               -----------------------------
/*БД «Кораблі». З таблиці Ships вивести назви кораблів, що мають у своїй назві дві літери 'e'.*/

SELECT [name] FROM [ships] WHERE [name] LIKE '%e%e%' AND [name] NOT LIKE '%e%e%e%'
GO 

--------------------------------------            32               -----------------------------
/*БД «Кораблі». З таблиці Ships вивести назви кораблів та роки їх спуску на воду, назва яких не закінчується на літеру 'a'.*/

SELECT [name],[launched] FROM [ships] WHERE [name] NOT LIKE '%a'
GO 

--------------------------------------            33               -----------------------------
/*БД «Кораблі». Вивести назви битв, які складаються з двох слів та друге слово не закінчується на літеру 'c'.*/

SELECT [name] FROM [battles] WHERE [name] LIKE '% %' AND [name] NOT LIKE '% %c'
GO 

--------------------------------------            34               -----------------------------
/*БД «Аеропорт». З таблиці Trip вивести інформацію про рейси, що вилітають в інтервалі часу між 12 та 17 годинами включно.*/


SELECT * FROM [trip] WHERE [time_out] BETWEEN '1900-01-01 12:00:00.000' AND '1900-01-01 17:00:00.000'
GO

--------------------------------------            35               -----------------------------
/*БД «Аеропорт». З таблиці Trip вивести інформацію про рейси, що прилітають в інтервалі часу між 17 та 23 годинами включно.*/

SELECT * FROM [trip] WHERE [time_in] BETWEEN '1900-01-01 17:00:00.000' AND '1900-01-01 23:00:00.000'
GO

--------------------------------------            36               -----------------------------
/*БД «Аеропорт». З таблиці Trip вивести інформацію про рейси, що прилітають в інтервалі часу між 21 та 10 годинами включно.*/

SELECT * FROM [trip] WHERE [time_in] >= '1900-01-01 21:00:00.000' OR [time_in] <= '1900-01-01 10:00:00.000'
GO

--------------------------------------            37               -----------------------------
/*БД «Аеропорт». З таблиці Pass_in_trip вивести дати, коли були зайняті місця у першому ряду.*/

SELECT DISTINCT [date] FROM [pass_in_trip] WHERE [place] LIKE '1%'
GO

--------------------------------------            38               -----------------------------
/*БД «Аеропорт». З таблиці Pass_in_trip вивести дати, коли були зайняті місця 'c' у будь-якому ряді.*/

SELECT DISTINCT [date] FROM [pass_in_trip] WHERE [place] LIKE '%c'
GO

--------------------------------------            39               -----------------------------
/*БД «Аеропорт». Вивести прізвища пасажирів (друге слово у стовпці name), що починаються на літеру 'С'.*/

SELECT [name] FROM [passenger] WHERE [name] LIKE '% C%'
GO

--------------------------------------            40               -----------------------------
/*БД «Аеропорт». Вивести прізвища пасажирів (друге слово у стовпці name), що не починаються на літеру 'J'.*/

SELECT [name] FROM [passenger] WHERE [name] NOT LIKE '% J%'
GO

--------------------------------------            41               -----------------------------
/*БД «Комп. фірма». Виведіть середню ціну ноутбуків з попереднім текстом 'середня ціна = '. */

SELECT CONCAT(N'середня ціна = ',  (SELECT AVG([price]) FROM [laptop]))
GO
--------------------------------------            42               -----------------------------
/*БД «Комп. фірма». Для таблиці PC вивести усю інформацію з коментарями у кожній комірці, наприклад, 'модель: 1121', 'ціна: 600,00' і т.д. */

SELECT CONCAT(N'код: ',CAST([code] AS VARCHAR(6))) AS [код],
	   CONCAT(N'модель: ',CAST([model] AS VARCHAR(6))) AS [модель],
	   CONCAT(N'частота: ',CAST([speed] AS VARCHAR(6))) AS [частота],
	   CONCAT(N'оперативка: ',CAST([ram] AS VARCHAR(6))) AS [оперативка],
	   CONCAT(N'жорсткий_диск: ',CAST([hd] AS VARCHAR(6))) AS [жорсткий_диск],
	   CONCAT(N'сд_привід: ',CAST([cd] AS VARCHAR(6))) AS [сд_привід],
	   CONCAT(N'ціна: ',CAST([price] AS VARCHAR(6))) AS [ціна]
FROM [pc]
GO

--------------------------------------            43               -----------------------------
/*БД «Фірма прий. вторсировини». З таблиці Income виведіть дати у такому форматі: рік.число_місяця.день, 
наприклад, 2001.02.15 (без формату часу).*/

SELECT 
	CASE 
		WHEN MONTH([date]) < 10 
			THEN CONCAT(CAST(YEAR([date]) AS VARCHAR(4)),'.0',CAST(MONTH([date]) AS VARCHAR(2)),'.',CAST(DAY([date]) AS VARCHAR(2)))
			ELSE CONCAT(CAST(YEAR([date]) AS VARCHAR(4)),'.',CAST(MONTH([date]) AS VARCHAR(2)),'.',CAST(DAY([date]) AS VARCHAR(2)))
	END AS [new_format] 
FROM [income] 
GO

--------------------------------------            44               -----------------------------
/*БД «Кораблі». Для таблиці Outcomes виведіть дані, а заміть значень стовпця result, 
виведіть еквівалентні їм надписи українською мовою.*/

SELECT [ship],[battle],
	CASE [result]
		WHEN 'OK' THEN 'без пошкоджень'
		WHEN 'sunk' THEN 'потоплений'
		WHEN 'damaged' THEN 'пошкоджений'
	END AS [результат]
 FROM [outcomes]
GO

--------------------------------------            45               -----------------------------
/*БД «Аеропорт». Для таблиці Pass_in_trip значення стовпця place розбити на два стовпця з коментарями, 
наприклад, перший – 'ряд: 2' та другий – 'місце: a'.*/

SELECT [trip_no],[date],[id_psg],CONCAT('ряд: ',CAST([place] AS CHAR(1))) AS [ряд],CONCAT('місце: ',RIGHT([place], 9)) AS [місце] 
FROM [pass_in_trip]
GO

--------------------------------------            46               -----------------------------
/*БД «Аеропорт». Вивести дані для таблиці Trip з об’єднаними значеннями двох стовпців: town_from та town_to, 
з додатковими коментарями типу: 'from Rostov to Paris'.*/

SELECT [trip_no],[id_comp],[plane],
		CONCAT('from ',RTRIM([town_from]),' to ',RTRIM ([town_to])) AS [route],
	   [time_out],[time_in]
FROM [trip]
GO

--------------------------------------            47               -----------------------------
/*БД «Аеропорт». Вивести для таблиці Trip об’єднане значення для усіх полів, 
що складається з перших та останніх символів (літера, цифра …) кожного поля.*/

SELECT CONCAT(LEFT(RTRIM([trip_no]),1),RIGHT(RTRIM([trip_no]),1),LEFT(RTRIM([id_comp]),1),RIGHT(RTRIM([id_comp]),1),
			  LEFT(RTRIM([plane]),1),RIGHT(RTRIM([plane]),1),LEFT(RTRIM([town_from]),1),RIGHT(RTRIM([town_from]),1),
			  LEFT(RTRIM([town_to]),1),RIGHT(RTRIM([town_to]),1),
			  LEFT(CAST([time_out] AS VARCHAR(25)),1),RIGHT(CAST([time_out] AS VARCHAR(25)),1),
			  LEFT(CAST([time_in] AS VARCHAR(25)),1),RIGHT(CAST([time_in] AS VARCHAR(25)),1)) AS [all]
FROM [trip]
GO

--------------------------------------            48               -----------------------------
/*БД «Комп. фірма». Знайдіть виробників, що випускають по крайній мірі дві різні моделі ПК. Вивести: maker, число моделей.  */

SELECT [maker],COUNT([maker])AS q FROM [product] WHERE [type] = 'pc' GROUP BY [maker] HAVING COUNT([maker]) >1
GO

--------------------------------------            49               -----------------------------
/*БД «Аеропорт». Для кожного міста порахувати кількість рейсів, що у ньому курсують (прилітають, відлітають).*/

SELECT SUM([trips_q]),[town] FROM 
(SELECT COUNT([trip_no]) AS [trips_q],[town_from] AS [town]  FROM [trip] GROUP BY [town_from]
	UNION ALL
SELECT COUNT([trip_no]) AS [trips_q],[town_to] AS [town] FROM [trip] GROUP BY [town_to]) AS q
GROUP BY [town]

--------------------------------------            50               -----------------------------
/*БД «Комп. фірма». Порахуйте для кожного типу принтера кількість наявних моделей.*/

SELECT [type],COUNT([model])AS q FROM [printer] GROUP BY [type] 
GO

--------------------------------------            51               -----------------------------
/*БД «Комп. фірма». Для моделей в таблиці PC порахуйте, скільки є різних cd (тобто з різною швидкістю), 
а також для кожної швидкості вкажіть наявну кількість моделей. */

SELECT z2.[cd], z2.quantiy_cd, COUNT(z2.[cd])AS quantiy_model FROM (SELECT [cd],[model] FROM [pc] GROUP BY [model],[cd]) AS z1 
	JOIN 
(SELECT [cd],COUNT([cd])AS quantiy_cd  FROM [pc]  GROUP BY [cd]) AS z2 ON z1.[cd]=z2.[cd] GROUP BY z2.[cd],z2.quantiy_cd
GO

/*як на мене попередній запит відповідає тому як сформульована умова, якщо би умова починалась з фраз "Для різних моделей..."
чи "Для кожної з моделей..." то відповідь мала б бути:*/

SELECT [model],[cd], COUNT(q.[cd]) AS [amount_model],COUNT(q.[model]) AS [amount_cd] 
FROM (SELECT [model],[cd] FROM [pc] GROUP BY [model],[cd]) AS q 
GROUP BY [model],[cd] WITH CUBE HAVING ([model] IS  NULL OR [cd] IS NULL) AND NOT ([model] IS  NULL AND [cd] IS NULL)
GO

--------------------------------------            52               -----------------------------
/*БД «Аеропорт». Для кожного рейсу (таблиця Trip) визначити тривалість його польоту.*/

SELECT CASE 
			WHEN [time_out] < [time_in]
				THEN CONCAT(CAST(FLOOR (DATEDIFF(MINUTE,[time_out],[time_in])/60)AS VARCHAR(2)),' hr  ',
							CAST((DATEDIFF(MINUTE,[time_out],[time_in])-FLOOR(DATEDIFF(MINUTE,[time_out],[time_in])/60)*60) AS VARCHAR(2)),' min')
				ELSE CONCAT(CAST(FLOOR ((24*60-DATEDIFF(MINUTE,[time_in],[time_out]))/60)AS VARCHAR(2)),' hr  ',
							CAST(((24*60-DATEDIFF(MINUTE,[time_in],[time_out]))-FLOOR((24*60-DATEDIFF(MINUTE,[time_in],[time_out]))/60)*60) AS VARCHAR(2)),' min')
		END AS [duration]
FROM [trip]
GO

--------------------------------------            53               -----------------------------
/*БД «Фірма прий. вторсировини». Для таблиці Outcome для кожного пункту порахувати суму грошей, як за кожне число,
так і за всі дати, а також вивести для них мінімальну та максимальну суми.*/

SELECT z1.*,[sum_per_point],[sum_per_day] FROM
	(SELECT [point],MAX([sum_per_day]) AS [max_sum],MIN([sum_per_day]) AS [min_sum] FROM
	(
		SELECT q1.[point] AS [point],[sum_per_point],[sum_per_day] FROM
			(SELECT [point], SUM([out]) AS [sum_per_point] FROM [outcome] GROUP BY  [point]) AS q1
				JOIN
			(SELECT [point],[date], SUM([out]) AS [sum_per_day] FROM [outcome] GROUP BY [date],[point]) AS q2
				ON q1.[point] = q2.[point]
		) AS a1 GROUP BY [point]
	) AS z1
		JOIN
	(SELECT q1.[point] AS [point],[sum_per_point],[sum_per_day] FROM
			(SELECT [point], SUM([out]) AS [sum_per_point] FROM [outcome] GROUP BY  [point]) AS q1
				JOIN
			(SELECT [point],[date], SUM([out]) AS [sum_per_day] FROM [outcome] GROUP BY [date],[point]) AS q2
				ON q1.[point] = q2.[point]
	) AS z2
		ON z1.[point] = z2.[point]
GO

--------------------------------------            54               -----------------------------
/*БД «Аеропорт». Для таблиці Pass_in_trip для кожного рейсу (trip_no) порахувати, скільки було зайнято місць у кожному ряді.*/

SELECT COUNT([id_psg]) AS [taken_places],[trip_no],[date],[row] FROM (SELECT [id_psg],[trip_no],[date],CAST([place] AS CHAR(1)) AS [row] FROM [pass_in_trip]) AS q
GROUP BY [trip_no],[date],[row]
GO

--------------------------------------            55               -----------------------------
/*БД «Аеропорт». Для таблиці Passenger порахувати скільки було пасажирів, прізвища яких починаються на літеру S, B та А.*/

SELECT COUNT([id_psg]) FROM [passenger] WHERE [name] LIKE ('% [SBA]%')
GO