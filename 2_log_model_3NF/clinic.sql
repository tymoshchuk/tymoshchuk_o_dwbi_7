use master
go

drop database if exists clinic

create database clinic
go

use clinic
go

drop table if exists person
go
drop table if exists doctor
go
drop table if exists visit
go
drop table if exists city
go
drop table if exists street
go
drop table if exists blood_type
go
drop table if exists occupation
go
drop table if exists allergy
go
drop table if exists disease
go
drop table if exists schedule
go
drop table if exists incapacitate_list
go
drop table if exists inherited_disease
go
drop table if exists happened_disease
go
drop table if exists allergic_trouble
go

create table person
(
id int not null primary key,
name varchar(20),
middle_name varchar(20),
surname varchar(20),
date_of_birth datetime,
photo image,
city_id int,
street_id int,
building int,
flat int,
responsible_doctor_id int,
blood_type_id int,
last_visit_id int,
phone_number int,
)
go

create table city
(
id int not null primary key,
city varchar(20),
)
go

create table street
(
id int not null primary key,
street varchar(30),
)
go

create table doctor
(
id int not null primary key,
name varchar(20),
middle_name varchar(20),
surname varchar(20),
room_number int,
occupation_id int,
schedule_id int,
)
go

create table blood_type
(
id int not null primary key,
blood_type varchar(6),
)
go

create table occupation
(
id int not null primary key,
occupation varchar(50),
)
go

create table schedule
(
id int not null primary key,
start_time date,
end_time date,
)
go

create table disease
(
id int not null primary key,
disease varchar(60),
)
go

create table allergy
(
id int not null primary key,
allergy varchar(60),
)
go

create table inherited_disease
(
id int not null primary key,
disease_id int,
person_id int,
)
go

create table happened_disease
(
id int not null primary key,
disease_id int,
person_id int,
)
go

create table allergic_trouble
(
id int not null primary key,
allergy_id int,
person_id int,
)
go

create table visit
(
id int not null primary key,
person_id int,
doctor_id int,
date_of_visit datetime,
complaint varchar(60),
symptom varchar(60),
diagnosis varchar(60),
prescription varchar(60),
medicine varchar(60)
)
go

create table incapacitate_list
(
id int not null primary key,
person_id int,
doctor_id int,
start_time date,
end_time date,
reason varchar(60)
)
go

alter table person
add constraint fk_person_city 
foreign key (city_id) references city(id)
go

alter table person
add constraint fk_person_street 
foreign key (street_id) references street(id)
go

alter table person
add constraint fk_person_responsible_doctor 
foreign key (responsible_doctor_id) references doctor(id)
go

alter table person
add constraint fk_person_blood_type 
foreign key (blood_type_id) references blood_type(id)
go

alter table person
add constraint fk_person_last_visit 
foreign key (last_visit_id) references visit(id)
go

alter table doctor
add constraint fk_doctor_occupation
foreign key (occupation_id) references occupation(id)
go

alter table doctor
add constraint fk_doctor_schedule
foreign key (schedule_id) references schedule(id)
go

alter table inherited_disease
add constraint fk_inherited_disease_person
foreign key (person_id) references person(id)
go

alter table inherited_disease
add constraint fk_inherited_disease_disease
foreign key (disease_id) references disease(id)
go

alter table happened_disease
add constraint fk_happened_disease_person
foreign key (person_id) references person(id)
go

alter table happened_disease
add constraint fk_happened_disease_disease
foreign key (disease_id) references disease(id)
go

alter table allergic_trouble
add constraint fk_allergic_trouble_person
foreign key (person_id) references person(id)
go

alter table allergic_trouble
add constraint fk_allergic_trouble_disease
foreign key (allergy_id) references allergy(id)
go

alter table visit
add constraint fk_visit_person
foreign key (person_id) references person(id)
go

alter table visit
add constraint fk_visit_doctor
foreign key (doctor_id) references doctor(id)
go

alter table incapacitate_list
add constraint fk_incapacitate_list_person
foreign key (person_id) references person(id)
go

alter table incapacitate_list
add constraint fk_incapacitate_list_doctor
foreign key (doctor_id) references doctor(id)
go
