﻿USE [O_Tymoshchuk_library]
GO

SELECT * FROM [author]
SELECT * FROM [book]
SELECT * FROM [book_author]
SELECT * FROM [publisher]
SELECT * FROM [author_log]
GO

UPDATE [book]
SET [title] = 'Tronka',
	[edition] = 1000
WHERE [publisher_id] = 1
GO

UPDATE [book]
SET [title] = 'Zacharovana Desna',
	[edition] = 1000
WHERE [publisher_id] = 5
GO

UPDATE [book]
SET [title] = 'Chorna Rada',
	[edition] = 1000
WHERE [publisher_id] = 3
GO

UPDATE [book]
SET [title] = 'Eneida',
	[edition] = 1000
WHERE [publisher_id] = 8
GO

INSERT INTO [book]
			([ISBN],
			[publisher_id],
			[URL],
			[price],
			[title],
			[edition]
			)
VALUES
			('9785265000214',1,'www.Veselka.com/9785265000214',78,'Tygrolovy',2000),
			('9785308005213',2,'www.Dnipro.com/9785308005213',28,'Sad Getsymanskyy', 5000),
			('9788671050636',3,'www.Veselka.com/9788671050636',45,'Tini zabutyh predkiv', 6000),
			('9785301014967',4,'www.Stavropigion.com/9785301014967',69,'Lys Mykyta', 10000),
			('9785457982351',4,'www.Smoloskyp.com/9785457982351',94,'Poviya', 5000),
			('9789661454609',5,'www.Shkola.com/9789661454609',39,'Voloymyr', 4000)
GO

INSERT INTO [author]
			([id],
			[name],
			[birhtday],
			[book_amount],
			[issue_amount],
			[total_edition]
			)
VALUES
			(21,'MyhayloStarytskyy',1840-05-24,18,27,156000),
			(22,'LeonidGlibov',1828-12-05,14,12,86000),
			(23,'MykolaGogol',1778-02-11,14,127,356000),
			(24,'LesyaUkrainka',1871-02-13,8,42,562000),
			(25,'TarasShevchenko',1814-03-09,11,225,7546000)
GO

INSERT INTO [book_author]
			([id],
			[ISBN],
			[author_id],
			[seq_no]
			)
VALUES
			(21,'9785265000214',21,10000),
			(22,'9785308005213',22,10000),
			(23,'9788671050636',23,10000),
			(24,'9785301014967',24,10000),
			(25,'9785457982351',25,10000)
GO

SELECT * FROM [author]
SELECT * FROM [book]
SELECT * FROM [book_author]
SELECT * FROM [publisher]
SELECT * FROM [author_log]
GO