﻿USE [O_Tymoshchuk_library]
GO

EXEC sp_configure 'show advanced options', 1;
GO
RECONFIGURE ;
GO
EXEC sp_configure 'nested triggers', 1 ;
GO
RECONFIGURE;
GO


DROP TRIGGER IF EXISTS [tr_modify_book_publisher]
DROP TRIGGER IF EXISTS [tr_modify_book_author_author]
GO

ALTER TRIGGER [tr_populate_author_log]
ON [author]
AFTER INSERT,DELETE,UPDATE
AS
BEGIN
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation NCHAR(6)
DECLARE @ins INT = (SELECT COUNT(*)FROM INSERTED)
DECLARE @del INT = (SELECT COUNT(*)FROM DELETED)
SET @operation = 
       CASE
			WHEN @ins>0 AND @del>0 THEN 'update'
			WHEN @ins>0 AND @del=0 THEN 'insert'
			WHEN @ins=0 AND @del>0 THEN 'delete'
	   END
IF @operation = 'insert'
	INSERT INTO[author_log]
		(
		[author_new_id],
		[name_new],
		[URL_new],
		[book_amount_new],
		[issue_amount_new],
		[total_edition_new],
		[operation_type]
		)
	SELECT [id],[name],[URL],[book_amount],[issue_amount],[total_edition],@operation FROM INSERTED
IF @operation = 'delete'
	INSERT INTO[author_log]
		(
		[author_old_id],
		[name_old],
		[URL_old],
		[book_amount_old],
		[issue_amount_old],
		[total_edition_old],
		[operation_type]
		)
	SELECT [id],[name],[URL],[book_amount],[issue_amount],[total_edition],@operation FROM DELETED
IF @operation = 'update'
	INSERT INTO[author_log]
		(
		[author_new_id],
		[name_new],
		[URL_new],
		[book_amount_new],
		[issue_amount_new],
		[total_edition_new],
		[author_old_id],
		[name_old],
		[URL_old],
		[book_amount_old],
		[issue_amount_old],
		[total_edition_old],
		[operation_type]
		)
	SELECT INSERTED.[id],INSERTED.[name],INSERTED.[URL],INSERTED.[book_amount],INSERTED.[issue_amount],INSERTED.[total_edition],
			DELETED.[id],DELETED.[name],DELETED.[URL],DELETED.[book_amount],DELETED.[issue_amount],DELETED.[total_edition],@operation 
	FROM INSERTED JOIN DELETED ON INSERTED.[id] = DELETED.[id]
UPDATE [author]
SET [author].[updated] = CURRENT_TIMESTAMP,
    [author].[updated_by] = SYSTEM_USER
FROM [author] JOIN INSERTED ON [author].[id] = INSERTED.[id]
END
GO

CREATE OR ALTER TRIGGER [tr_modify_book_publisher]
ON [book]
AFTER UPDATE,DELETE,INSERT
AS
BEGIN
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation NCHAR(6)
DECLARE @ins INT = (SELECT COUNT(*)FROM INSERTED)
DECLARE @del INT = (SELECT COUNT(*)FROM DELETED)
SET @operation = 
       CASE
			WHEN @ins>0 AND @del>0 THEN 'update'
			WHEN @ins>0 AND @del=0 THEN 'insert'
			WHEN @ins=0 AND @del>0 THEN 'delete'
	   END
IF @operation = 'insert'
UPDATE [publisher]
	SET 
		[publisher].[book_amount] = [publisher].[book_amount] + (SELECT COUNT(INSERTED.[ISBN]) FROM INSERTED JOIN [publisher] ON INSERTED.[publisher_id] = [publisher].[id]),
		[publisher].[issue_amount] = [publisher].[issue_amount] + (SELECT COUNT(INSERTED.[ISBN]) FROM INSERTED JOIN [publisher] ON INSERTED.[publisher_id] = [publisher].[id]),
		[publisher].[total_edition] = [publisher].[total_edition] + (SELECT SUM(INSERTED.[edition]) FROM INSERTED JOIN [publisher] ON INSERTED.[publisher_id] = [publisher].[id])
IF @operation = 'delete'
UPDATE [publisher]
	SET 
		[publisher].[book_amount] = [publisher].[book_amount] - (SELECT COUNT(DELETED.[ISBN]) FROM DELETED JOIN [publisher] ON DELETED.[publisher_id] = [publisher].[id]),
		[publisher].[issue_amount] = [publisher].[issue_amount] - (SELECT COUNT(DELETED.[ISBN]) FROM DELETED JOIN [publisher] ON DELETED.[publisher_id] = [publisher].[id]),
		[publisher].[total_edition] = [publisher].[total_edition] - (SELECT SUM(DELETED.[edition]) FROM DELETED JOIN [publisher] ON DELETED.[publisher_id] = [publisher].[id])
IF @operation = 'update'
UPDATE [publisher]
	SET 
		[publisher].[book_amount] = [publisher].[book_amount] + (SELECT COUNT(INSERTED.[ISBN]) FROM INSERTED JOIN [publisher] ON INSERTED.[publisher_id] = [publisher].[id])-
			(SELECT COUNT(DELETED.[ISBN]) FROM DELETED JOIN [publisher] ON DELETED.[publisher_id] = [publisher].[id]),
		[publisher].[issue_amount] = [publisher].[issue_amount] + (SELECT COUNT(INSERTED.[ISBN]) FROM INSERTED JOIN [publisher] ON INSERTED.[publisher_id] = [publisher].[id])-
			(SELECT COUNT(DELETED.[ISBN]) FROM DELETED JOIN [publisher] ON DELETED.[publisher_id] = [publisher].[id]),
		[publisher].[total_edition] = [publisher].[total_edition] + (SELECT SUM(INSERTED.[edition]) FROM INSERTED JOIN [publisher] ON INSERTED.[publisher_id] = [publisher].[id])-
			(SELECT SUM(DELETED.[edition]) FROM DELETED JOIN [publisher] ON DELETED.[publisher_id] = [publisher].[id])
END
GO


CREATE OR ALTER TRIGGER [tr_modify_book_author_author]
ON [book_author]
AFTER UPDATE,DELETE,INSERT
AS
BEGIN
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation NCHAR(6)
DECLARE @ins INT = (SELECT COUNT(*)FROM INSERTED)
DECLARE @del INT = (SELECT COUNT(*)FROM DELETED)
SET @operation = 
       CASE
			WHEN @ins>0 AND @del>0 THEN 'update'
			WHEN @ins>0 AND @del=0 THEN 'insert'
			WHEN @ins=0 AND @del>0 THEN 'delete'
	   END
IF @operation = 'insert'
UPDATE [author]
	SET 
		[author].[book_amount] = [author].[book_amount] + (SELECT COUNT(INSERTED.[ISBN]) FROM INSERTED JOIN [author] ON INSERTED.[author_id] = [author].[id]),
		[author].[issue_amount] = [author].[issue_amount] + (SELECT COUNT(INSERTED.[ISBN]) FROM INSERTED JOIN [author] ON INSERTED.[author_id] = [author].[id]),
		[author].[total_edition] = [author].[total_edition] + (SELECT SUM(INSERTED.[seq_no]) FROM INSERTED JOIN [author] ON INSERTED.[author_id] = [author].[id])
IF @operation = 'delete'
UPDATE [author]
	SET 
		[author].[book_amount] = [author].[book_amount] - (SELECT COUNT(DELETED.[ISBN]) FROM DELETED JOIN [author] ON DELETED.[author_id] = [author].[id]),
		[author].[issue_amount] = [author].[issue_amount] - (SELECT COUNT(DELETED.[ISBN]) FROM DELETED JOIN [author] ON DELETED.[author_id] = [author].[id]),
		[author].[total_edition] = [author].[total_edition] - (SELECT SUM(DELETED.[seq_no]) FROM DELETED JOIN [author] ON DELETED.[author_id] = [author].[id])
IF @operation = 'update'
UPDATE [author]
	SET 
		[author].[book_amount] = [author].[book_amount] + (SELECT COUNT(INSERTED.[ISBN]) FROM INSERTED JOIN [author] ON INSERTED.[author_id] = [author].[id])-
			(SELECT COUNT(DELETED.[ISBN]) FROM DELETED JOIN [author] ON DELETED.[author_id] = [author].[id]),
		[author].[issue_amount] = [author].[issue_amount] + (SELECT COUNT(INSERTED.[ISBN]) FROM INSERTED JOIN [author] ON INSERTED.[author_id] = [author].[id])-
			(SELECT COUNT(DELETED.[ISBN]) FROM DELETED JOIN [author] ON DELETED.[author_id] = [author].[id]),
		[author].[total_edition] = [author].[total_edition] + (SELECT SUM(INSERTED.[seq_no]) FROM INSERTED JOIN [author] ON INSERTED.[author_id] = [author].[id])-
			(SELECT SUM(DELETED.[seq_no]) FROM DELETED JOIN [author] ON DELETED.[author_id] = [author].[id])
END
GO