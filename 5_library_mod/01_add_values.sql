﻿USE [O_Tymoshchuk_library]
GO

ALTER TABLE [author] ADD
						[birhtday] DATETIME NULL,
						[book_amount] INT NOT NULL DEFAULT (0) CHECK ([book_amount] >= 0),
						[issue_amount] INT NOT NULL DEFAULT (0) CHECK ([issue_amount] >= 0),
						[total_edition] INT NOT NULL DEFAULT (0) CHECK ([total_edition] >= 0)
GO

ALTER TABLE [book] ADD
						[title] NVARCHAR(90) NOT NULL DEFAULT ('Title'),
						[edition] INT NOT NULL DEFAULT (1) CHECK ([edition] >= 1),
						[published] DATETIME NULL
GO

ALTER TABLE [publisher] ADD
						[created] DATETIME NOT NULL DEFAULT (1900-01-01),
						[country] NVARCHAR(30) NOT NULL DEFAULT ('USA'),
						[city] NVARCHAR(30) NOT NULL DEFAULT ('NY'),
						[book_amount] INT NOT NULL DEFAULT (0) CHECK ([book_amount] >= 0),
						[issue_amount] INT NOT NULL DEFAULT (0) CHECK ([issue_amount] >= 0),
						[total_edition] INT NOT NULL DEFAULT (0) CHECK ([total_edition] >= 0)
GO

ALTER TABLE [author_log] ADD
						[book_amount_old] INT NULL,
						[issue_amount_old] INT NULL,
						[total_edition_old] INT NULL,
						[book_amount_new] INT NULL,
						[issue_amount_new] INT NULL,
						[total_edition_new] INT NULL
GO