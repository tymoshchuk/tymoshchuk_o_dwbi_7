﻿USE [O_Tymoshchuk_library]
GO

SELECT * FROM [author]
SELECT * FROM [book]
SELECT * FROM [book_author]
SELECT * FROM [publisher]
SELECT * FROM [author_log]
GO

UPDATE [author]
SET [name] = 'PavloTychyna'
WHERE [id] = 2
GO

UPDATE [author]
SET [name] = 'PlatonVoronko'
WHERE [id] = 4
GO

UPDATE [author]
SET [name] = 'MaksymRylskyy'
WHERE [id] = 6
GO

UPDATE [author]
SET [name] = 'AndriyMalyshko'
WHERE [id] = 8
GO

UPDATE [author]
SET [name] = 'VasylSymonenko'
WHERE [id] = 10
GO

UPDATE [author]
SET [name] = 'NatalyaZabila'
WHERE [id] = 12
GO

UPDATE [author]
SET [inserted_by] = 'tymoshchuk'
WHERE [id] in (14,16,18,20)
GO

 -----------------------------------------------

UPDATE [publisher]
SET [name] = CONCAT('Radyanska',[name])
WHERE [id] in (3,7,8,15,20)
GO

UPDATE [publisher]
SET [name] = CONCAT([name],'Ukrayiny')
WHERE [id] in (5,6,7,10,16)
GO

-----------------------------------------------

UPDATE [book]
SET [price] = [price]*0.9
WHERE [price] >50
GO

----------------------------------------------

UPDATE [book_author]
SET [seq_no] = [seq_no]*[id]
WHERE [id] in (1,4,5,6,7,10,11,12,15,16)
GO

SELECT * FROM [author]
SELECT * FROM [book]
SELECT * FROM [book_author]
SELECT * FROM [publisher]
SELECT * FROM [author_log]
GO