﻿USE [O_Tymoshchuk_library_synonymes]
GO

DROP SYNONYM IF EXISTS [author_synonym]
DROP SYNONYM IF EXISTS [book_synonym]
DROP SYNONYM IF EXISTS [book_author_synonym]
DROP SYNONYM IF EXISTS [publisher_synonym]
DROP SYNONYM IF EXISTS [author_log_synonym]
GO

CREATE SYNONYM [author_synonym] FOR [O_Tymoshchuk_library].[dbo].[author]
GO

CREATE SYNONYM [book_synonym] FOR [O_Tymoshchuk_library].[dbo].[book]
GO

CREATE SYNONYM [book_author_synonym] FOR [O_Tymoshchuk_library].[dbo].[book_author]
GO

CREATE SYNONYM [publisher_synonym] FOR [O_Tymoshchuk_library].[dbo].[publisher]
GO

CREATE SYNONYM [author_log_synonym] FOR [O_Tymoshchuk_library].[dbo].[author_log]
GO