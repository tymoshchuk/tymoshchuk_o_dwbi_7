﻿USE [O_Tymoshchuk_library_view]
GO

DROP VIEW IF EXISTS [author_view]
DROP VIEW IF EXISTS [book_view]
DROP VIEW IF EXISTS [book_author_view]
DROP VIEW IF EXISTS [publisher_view]
DROP VIEW IF EXISTS [author_log_view]
GO

CREATE VIEW [author_view]
AS
SELECT * FROM [O_Tymoshchuk_library].[dbo].[author]
GO

CREATE VIEW [book_view]
AS
SELECT * FROM [O_Tymoshchuk_library].[dbo].[book]
GO

CREATE VIEW [book_author_view]
AS
SELECT * FROM [O_Tymoshchuk_library].[dbo].[book_author]
GO

CREATE VIEW [publisher_view]
AS
SELECT * FROM [O_Tymoshchuk_library].[dbo].[publisher]
GO

CREATE VIEW [author_log_view]
AS
SELECT * FROM [O_Tymoshchuk_library].[dbo].[author_log]
GO