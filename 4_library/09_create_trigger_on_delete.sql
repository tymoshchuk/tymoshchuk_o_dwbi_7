﻿USE [O_Tymoshchuk_library]
GO

DROP TRIGGER IF EXISTS [tr_delete_author_log]
GO

CREATE OR ALTER TRIGGER [tr_delete_author_log]
ON [author_log]
INSTEAD OF DELETE
AS
BEGIN
PRINT 'Operation is forbidden'
END
GO
