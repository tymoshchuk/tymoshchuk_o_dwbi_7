﻿USE master
GO

DROP DATABASE IF EXISTS [O_Tymoshchuk_library]
GO

CREATE DATABASE [O_Tymoshchuk_library]
ON PRIMARY
(NAME = 'Main', FILENAME = 'F:\DTBS\Main.mdf'),

FILEGROUP [DATA] DEFAULT
(NAME = 'DATA', FILENAME = 'F:\DTBS\DATA.ndf')

LOG ON 
(NAME = 'Main_log', FILENAME = 'F:\DTBS\Main_log.ldf')
GO
