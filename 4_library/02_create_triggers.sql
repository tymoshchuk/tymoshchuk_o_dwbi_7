﻿USE [O_Tymoshchuk_library]
GO

DROP TRIGGER IF EXISTS [tr_update_book]
DROP TRIGGER IF EXISTS [tr_update_book_author]
DROP TRIGGER IF EXISTS [tr_update_publisher]
DROP TRIGGER IF EXISTS [tr_populate_author_log]
GO


CREATE OR ALTER TRIGGER [tr_update_book]
ON [book]
AFTER UPDATE
AS
BEGIN
IF @@ROWCOUNT = 0 RETURN
UPDATE [book]
SET [book].[updated] = CURRENT_TIMESTAMP,
    [book].[updated_by] = SYSTEM_USER
FROM [book] JOIN INSERTED ON [book].[ISBN] = INSERTED.[ISBN]
END
GO

CREATE OR ALTER TRIGGER [tr_update_book_author]
ON [book_author]
AFTER UPDATE
AS
BEGIN
IF @@ROWCOUNT = 0 RETURN
UPDATE [book_author]
SET [book_author].[updated] = CURRENT_TIMESTAMP,
    [book_author].[updated_by] = SYSTEM_USER
FROM [book_author] JOIN INSERTED ON [book_author].[id] = INSERTED.[id]
END
GO

CREATE OR ALTER TRIGGER [tr_update_publisher]
ON [publisher]
AFTER UPDATE
AS
BEGIN
IF @@ROWCOUNT = 0 RETURN
UPDATE [publisher]
SET [publisher].[updated] = CURRENT_TIMESTAMP,
    [publisher].[updated_by] = SYSTEM_USER
FROM [publisher] JOIN INSERTED ON [publisher].[id] = INSERTED.[id]
END
GO

CREATE OR ALTER TRIGGER [tr_populate_author_log]
ON [author]
AFTER INSERT,DELETE,UPDATE
AS
BEGIN
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation NCHAR(6)
DECLARE @ins INT = (SELECT COUNT(*)FROM INSERTED)
DECLARE @del INT = (SELECT COUNT(*)FROM DELETED)
SET @operation = 
       CASE
			WHEN @ins>0 AND @del>0 THEN 'update'
			WHEN @ins>0 AND @del=0 THEN 'insert'
			WHEN @ins=0 AND @del>0 THEN 'delete'
	   END
IF @operation = 'insert'
	INSERT INTO[author_log]
		(
		[author_new_id],
		[name_new],
		[URL_new],
		[operation_type]
		)
	SELECT [id],[name],[URL],@operation FROM INSERTED
IF @operation = 'delete'
	INSERT INTO[author_log]
		(
		[author_old_id],
		[name_old],
		[URL_old],
		[operation_type]
		)
	SELECT [id],[name],[URL],@operation FROM DELETED
IF @operation = 'update'
	INSERT INTO[author_log]
		(
		[author_new_id],
		[name_new],
		[URL_new],
		[author_old_id],
		[name_old],
		[URL_old],
		[operation_type]
		)
	SELECT INSERTED.[id],INSERTED.[name],INSERTED.[URL],DELETED.[id],DELETED.[name],DELETED.[URL],@operation 
	FROM INSERTED JOIN DELETED ON INSERTED.[id] = DELETED.[id]
UPDATE [author]
SET [author].[updated] = CURRENT_TIMESTAMP,
    [author].[updated_by] = SYSTEM_USER
FROM [author] JOIN INSERTED ON [author].[id] = INSERTED.[id]
END
GO
