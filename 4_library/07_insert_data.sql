﻿USE [O_Tymoshchuk_library]
GO

SELECT * FROM [author]
SELECT * FROM [book]
SELECT * FROM [book_author]
SELECT * FROM [publisher]
SELECT * FROM [author_log]
GO


INSERT INTO [author]
			([id],
			[name]
			)
VALUES
			(1,'OlesGonchar'),
			(2,'PavloZagrebelnyy'),
			(3,'VolodymyrVynnychenko'),
			(4,'IvanBagryanyy'),
			(5,'OleksandrDovzhenko'),
			(6,'LinaKostenko'),
			(7,'MykolaHvylyovyy'),
			(8,'YuriyOlesha'),
			(9,'MyhayloKotsyubynskyy'),
			(10,'OlgaKobylyanska'),
			(11,'VasylStus'),
			(12,'IvanFranko'),
			(13,'SemenSklyarenko'),
			(14,'DmytroDontsov'),
			(15,'IvanKotlyarevskyy'),
			(16,'GrygoriySkovoroda'),
			(17,'OleksandrOles'),
			(18,'VasylStefanyk'),
			(19,'PanteleymonKulish'),
			(20,'OstapVyshnya')
GO

INSERT INTO [publisher]
			([id],
			[name]
			)
VALUES
			(1,'Veselka'),
			(2,'Kalvaria'),
			(3,'Shkola'),
			(4,'Stavropigion'),
			(5,'Grani'),
			(6,'Litopys'),
			(7,'Molod'),
			(8,'NaukovaDumka'),
			(9,'Osnovy'),
			(10,'Osvita'),
			(11,'Svichado'),
			(12,'Smoloskyp'),
			(13,'Syayvo'),
			(14,'Folio'),
			(15,'ChervonaKalyna'),
			(16,'Chas'),
			(17,'Kamenyar'),
			(18,'Znannya'),
			(19,'Dnipro'),
			(20,'Akademia')
GO

INSERT INTO [book]
			([ISBN],
			[publisher_id],
			[URL],
			[price]
			)
VALUES
			('9785265000217',1,'www.Veselka.com/9785265000217',78),
			('9785308005216',2,'www.Dnipro.com/9785308005216',28),
			('9788671050630',3,'www.Veselka.com/9788671050630',45),
			('9785301014963',4,'www.Stavropigion.com/9785301014963',69),
			('9785457982352',4,'www.Smoloskyp.com/9785457982352',94),
			('9789661454605',5,'www.Shkola.com/9789661454605',39),
			('9785170801954',6,'www.Veselka.com/9785170801954',78),
			('9789661492881',7,'www.Kamenyar.com/9789661492881',56),
			('9785991033077',8,'www.Folio.com/9785991033077',84),
			('9789669759689',9,'www.Molod.com/9789669759689',49),
			('9785447554897',10,'www.Akademia.com/9785447554897',38),
			('9786176793021',11,'www.Osnovy.com/9786176793021',93),
			('9789669695802',12,'www.Osnovy.com/9789669695802',74),
			('9789660377103',13,'www.Smoloskyp.com/9789660377103',62),
			('9780099597810',14,'www.Kalvaria.com/9780099597810',82),
			('9789660356924',15,'www.Znannya.com/9789660356924',52),
			('9786171217843',16,'www.Folio.com/9786171217843',70),
			('9786171214903',17,'www.Litopys.com/9786171214903',18),
			('9786177498994',19,'www.Syayvo.com/9786177498994',59),
			('9789663398402',20,'www.Dnipro.com/9789663398402',45)
GO

INSERT INTO [book_author]
			([id],
			[ISBN],
			[author_id],
			[seq_no]
			)
VALUES
			(1,'9785265000217',1,10000),
			(2,'9785308005216',2,10000),
			(3,'9788671050630',3,10000),
			(4,'9785301014963',4,10000),
			(5,'9785457982352',5,10000),
			(6,'9789661454605',6,10000),
			(7,'9785170801954',7,10000),
			(8,'9789661492881',8,10000),
			(9,'9785991033077',9,10000),
			(10,'9789669759689',10,10000),
			(11,'9785447554897',11,10000),
			(12,'9786176793021',12,10000),
			(13,'9789669695802',13,10000),
			(14,'9789660377103',14,10000),
			(15,'9780099597810',15,10000),
			(16,'9789660356924',16,10000),
			(17,'9786171217843',17,10000),
			(18,'9786171214903',18,10000),
			(19,'9786177498994',19,10000),
			(20,'9789663398402',20,10000)
GO

SELECT * FROM [author]
SELECT * FROM [book]
SELECT * FROM [book_author]
SELECT * FROM [publisher]
SELECT * FROM [author_log]
GO