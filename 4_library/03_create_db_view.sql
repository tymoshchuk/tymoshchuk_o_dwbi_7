﻿USE master
GO

DROP DATABASE IF EXISTS [O_Tymoshchuk_library_view]
GO

CREATE DATABASE [O_Tymoshchuk_library_view] 
ON PRIMARY
(NAME = 'View', FILENAME = 'F:\DTBS\View.mdf')
GO