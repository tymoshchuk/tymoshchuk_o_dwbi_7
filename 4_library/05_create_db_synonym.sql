﻿USE master
GO

DROP DATABASE IF EXISTS [O_Tymoshchuk_library_synonymes]
GO

CREATE DATABASE [O_Tymoshchuk_library_synonymes]
ON PRIMARY
(NAME = 'Synonym', FILENAME = 'F:\DTBS\Synonym.mdf')
GO