﻿USE [O_Tymoshchuk_library]
GO

SELECT * FROM [author]
SELECT * FROM [book]
SELECT * FROM [book_author]
SELECT * FROM [publisher]
SELECT * FROM [author_log]
GO

DELETE FROM [book_author]
WHERE [id] > 14
GO

DELETE FROM [book]
WHERE [publisher_id] > 14
GO

DELETE FROM [author]
WHERE [id] > 14
GO

DELETE FROM [publisher]
WHERE [id] > 14
GO

DELETE FROM [author_log]
WHERE [operation_id] > 14
GO

SELECT * FROM [author]
SELECT * FROM [book]
SELECT * FROM [book_author]
SELECT * FROM [publisher]
SELECT * FROM [author_log]
GO