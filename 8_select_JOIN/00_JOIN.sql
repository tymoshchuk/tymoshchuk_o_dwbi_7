﻿USE [labor_sql];
GO

--------------------------------------            1               -----------------------------
/*
БД «Комп. фірма». Вкажіть виробника для тих ПК, що мають жорсткий диск об’ємом не більше 8 Гбайт. Вивести: maker, type, speed, hd.*/

SELECT DISTINCT [maker],[type],[speed],[hd] FROM  [product] JOIN [pc] ON [product].[model] = [pc].[model]  WHERE [hd] <= 8
GO

--------------------------------------            2               -----------------------------
/*БД «Комп. фірма». Знайдіть виробників ПК з процесором не менше 600 МГц. Вивести: maker. */

SELECT DISTINCT [maker] FROM  [product] JOIN [pc] ON [product].[model] = [pc].[model]  WHERE [speed] >= 600
GO

--------------------------------------            3               -----------------------------
/*БД «Комп. фірма». Знайдіть виробників ноутбуків з процесором не вище 500 МГц. Вивести: maker. */

SELECT DISTINCT [maker] FROM  [product] JOIN [laptop] ON [product].[model] = [laptop].[model]  WHERE [speed] <= 500
GO

--------------------------------------            4               -----------------------------
/*БД «Комп. фірма». Знайдіть пари моделей ноутбуків, що мають однакові об’єми жорстких дисків та RAM (таблиця Laptop). 
У результаті кожна пара виводиться лише один раз. 
Порядок виведення: модель з більшим номером, модель з меншим номером, об’єм диску та RAM. */


SELECT DISTINCT L1.[model], L2.[model],L1.[ram]  FROM [laptop] AS L1,[laptop] AS L2 WHERE L1.[ram] = L2.[ram] AND L1.[model] > L2.[model]
GO

/*виправив помилку*/

SELECT DISTINCT L1.[model], L2.[model],L1.[ram]  FROM [laptop] AS L1,[laptop] AS L2 
WHERE (L1.[ram] = L2.[ram] OR L1.[hd] = L2.[hd])AND L1.[model] > L2.[model] 
GO

--------------------------------------            5               -----------------------------
/*БД «Кораблі». Знайдіть країни, що мали класи як звичайних бойових кораблів 'bb', так і класи крейсерів 'bc'. 
Вивести: country, типи з класом 'bb', типи з класом 'bc'.*/

SELECT q1.[country], q1.[class],q2.[class] FROM (SELECT * FROM [classes] WHERE [type] = 'bc') AS q1 
						JOIN 
					(SELECT * FROM [classes] WHERE [type] = 'bb') AS q2 
						ON q1.[country] = q2.[country]
GO

--------------------------------------            6               -----------------------------
/*БД «Комп. фірма». Знайдіть номер моделі та виробника ПК, яка має ціну менше за 600 дол. Вивести: model, makeк*/

SELECT DISTINCT [pc].[model],[maker]  FROM  [product] JOIN [pc] ON [product].[model] = [pc].[model]  WHERE [price] < 600
GO

--------------------------------------            7               -----------------------------
/*БД «Комп. фірма». Знайдіть номер моделі та виробника прінтера, яка має ціну вищу за 300 дол. Вивести: model, maker.*/

SELECT DISTINCT [printer].[model],[maker]  FROM  [product] JOIN [printer] ON [product].[model] = [printer].[model]  WHERE [price] > 300
GO

--------------------------------------            8               ---------------------------------
/*БД «Комп. фірма». Виведіть виробника, номер моделі та ціну кожного комп’ютера, що є у БД. Вивести: maker, model, price.*/

SELECT [product].[model],[maker],[price] FROM [product] LEFT JOIN [pc] ON [product].[model] = [pc].[model] 
	WHERE  [product].[type] = 'pc'
 UNION
SELECT [product].[model],[maker],[price] FROM [product] LEFT JOIN [laptop] ON [product].[model] = [laptop].[model] 
	WHERE  [product].[type] = 'laptop'
GO

--------------------------------------            9               -----------------------------
/*БД «Комп. фірма». Виведіть усі можливі моделі ПК, їх виробників та ціну (якщо вона вказана). Вивести: maker, model, price. */

SELECT DISTINCT [maker],[product].[model],[price] FROM [product] LEFT JOIN [pc] ON [product].[model] = [pc].[model] 
	WHERE  [product].[type] = 'pc'
GO

--------------------------------------            10               -----------------------------
/* БД «Комп. фірма». Виведіть виробника, тип, модель та частоту процесора для ноутбуків, частота процесорів яких перевищує 600 МГц. 
Вивести: maker, type, model, speed. */

SELECT [maker],[type],[product].[model],[speed] FROM [product] JOIN [laptop] ON [product].[model] = [laptop].[model] WHERE  [speed] > 600
GO

--------------------------------------            11               -----------------------------
/*БД «Кораблі». Для кораблів таблиці Ships вивести їх водотоннажність.*/

SELECT [name],[displacement] FROM [ships] JOIN [classes] ON [ships].[class] = [classes].[class]
GO

--------------------------------------            12               -----------------------------
/*БД «Кораблі». Для кораблів, що вціліли у битвах, вивести назви та дати битв, у яких вони брали участь.*/

SELECT [ship],[name],[date] FROM [outcomes] JOIN [battles] ON [outcomes].[battle] = [battles].[name]
WHERE [result] <> 'sunk'
GO

--------------------------------------            13               -----------------------------
/*БД «Кораблі». Для кораблів таблиці Ships вивести країни, яким вони належать.*/

SELECT [name],[country] FROM [ships] JOIN [classes] ON [ships].[class] = [classes].[class]
GO

--------------------------------------            14               -----------------------------
/*БД «Аеропорт». Для рейсових літаків 'Boeing' вказати назви компаній, яким вони належать.*/

SELECT DISTINCT [name] FROM [trip] JOIN [company] ON [trip].[id_comp] = [company].[id_comp] WHERE [plane] = 'Boeing'
GO

--------------------------------------            15              -----------------------------
/*БД «Аеропорт». Для пасажирів таблиці Passenger вивести дати, коли вони користувалися послугами авіаліній.*/

SELECT [date],[name] FROM [passenger] JOIN [pass_in_trip] ON [passenger].[id_psg] = [pass_in_trip].[id_psg]
GROUP BY [passenger].[name],[date]
GO

--------------------------------------            16               -----------------------------
/*БД «Комп. фірма». Знайти модель, частоту процесора та об’єм жорсткого диску для тих комп’ютерів, що комплектуються 
накопичувачами 10 або 20 Мб та випускаються виробником 'A'. 
Вивести: model, speed, hd. Вихідні дані впорядкувати за зростанням за стовпцем speed. */


SELECT q1.[model],[speed],[hd] FROM (SELECT * FROM [product] WHERE [maker] = 'A') AS q1 
JOIN (SELECT * FROM [pc] WHERE  [hd] IN (10,20))AS q2 ON q1.[model] = q2.[model] 
 UNION
SELECT q1.[model],[speed],[hd] FROM (SELECT * FROM [product] WHERE [maker] = 'A') AS q1 
JOIN (SELECT * FROM [laptop] WHERE  [hd] IN (10,20))AS q2 ON q1.[model] = q2.[model]
GO

--------------------------------------            17               -----------------------------
/*Для кожного виробника з таблиці Product визначити число моделей кожного типу продукції (PIVOT).*/

SELECT * FROM (SELECT [maker],[type],[model] AS model_available FROM [product]) AS info 
PIVOT 
(COUNT(model_available) FOR [type] IN ([pc],[laptop],[printer])) AS pvt_table
GO

--------------------------------------            18               -----------------------------
/*Порахувати середню ціну на ноутбуки в залежності від розміру екрана. (PIVOT)*/

SELECT * FROM (SELECT 'averege_price' AS avg_, [screen],[price] AS avg_price FROM [laptop]) AS info 
PIVOT 
(AVG(avg_price) FOR [screen] IN ([11],[12],[14],[15])) AS pvt_table
GO

--------------------------------------            19               -----------------------------
/*Для кожного ноутбука додатково вивести ім'я виробника. (CROSS APPLY) */

SELECT * FROM [laptop] CROSS APPLY (SELECT [maker] FROM [product] WHERE [product].[model] = [laptop].[model]) AS q

--------------------------------------            20               -----------------------------
/*Для кожного ноутбука додатково вивести максимальну ціну серед ноутбуків того ж виробника. (CROSS APPLY)*/

SELECT [code], lap.[model],[speed],[ram],[hd],[price],[screen],max_price FROM 
(SELECT * FROM [laptop] CROSS APPLY (SELECT [maker] FROM [product] WHERE [product].[model] = [laptop].[model]) AS q) AS lap 
CROSS APPLY 
(SELECT * FROM (SELECT [maker], MAX([price]) AS max_price  FROM [laptop] 
JOIN [product] ON [product].[model] = [laptop].[model] GROUP BY [product].[maker]) AS price_table 
WHERE lap.[maker] = price_table.[maker]) AS n 
GO

--------------------------------------            21               -----------------------------
/*З'єднати кожен рядок з таблиці Laptop з наступним рядком в порядку, заданому сортуванням (model, code). (CROSS APPLY) */ 

SELECT * FROM (SELECT TOP(SELECT COUNT(*) FROM [laptop]) * FROM [laptop] ORDER BY [model],[code]) AS q1 
CROSS APPLY 
(SELECT TOP 1 * FROM (SELECT TOP(SELECT COUNT(*) FROM [laptop]) * FROM [laptop] ORDER BY [model],[code]) AS q2 
 WHERE q1.[model] < q2.[model] OR q1.[model] = q2.[model] AND q1.[code] < q2.[code]) AS q
GO

--------------------------------------            22               -----------------------------
/*З'єднати кожен рядок з таблиці Laptop з наступним рядком впорядку, заданому сортуванням (model, code). (OUTER APPLY)*/

SELECT * FROM (SELECT TOP(SELECT COUNT(*) FROM [laptop]) * FROM [laptop] ORDER BY [model],[code]) AS q1 
OUTER APPLY 
(SELECT TOP 1 * FROM (SELECT TOP(SELECT COUNT(*) FROM [laptop]) * FROM [laptop] ORDER BY [model],[code]) AS q2  
WHERE q1.[model] < q2.[model] OR q1.[model] = q2.[model] AND q1.[code] < q2.[code]) AS q
GO

--------------------------------------            23               -----------------------------
/*Вивести з таблиці Product по три моделі з найменшими номерами з кожної групи, яка характеризується типом продукції. (CROOSS APPLY)*/

SELECT q.[maker],q.[model],q.[type] FROM (SELECT [type] FROM [product] GROUP BY [type]) AS q1 
CROSS APPLY 
(SELECT TOP 3 * FROM (SELECT TOP (SELECT COUNT(*) FROM [product]) * FROM [product] ORDER BY [type],[model])AS q2
WHERE q1.[type] = q2.[type] ) AS q
GO

--------------------------------------            24               -----------------------------
/*Для таблиці Laptop представити інформацію про продукти в
три стовпці: code, назва характеристики (speed, ram, hd або
screen), значення характеристики .. (CROOSS APPLY)*/

SELECT [code],[column] AS [name],[value] FROM 
(SELECT [code],[speed],[ram],CAST([hd] AS SMALLINT) AS [hd],CAST([screen] AS SMALLINT) AS [screen] FROM [laptop]) AS info 
UNPIVOT 
([value] FOR  [column] IN ([speed],[ram],[hd],[screen])) AS pvt_table
GO

/*поки що тільки через UNPIVOT, через (CROOSS APPLY) ше не придумав*/