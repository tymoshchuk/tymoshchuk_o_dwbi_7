﻿use [people_ua_db]
go

drop table if exists [name_list_identity]
go
create table [name_list_identity](
[id]		int		 identity, 
[name]	nvarchar(20) not null,
[sex]	nchar(1) null,
CONSTRAINT PK_name_list_identity Primary Key ([id])
)
go
insert into [name_list_identity]  ([name], [sex])
values 
(N'Буревій','m'),
(N'Даромира','w'),
(N'Водотрав','m'),
(N'Миробог','m'),
(N'Хівря','w'),
(N'Світокол','m'),
(N'Рід','m'),
(N'Жито','m'),
(N'Благослава','w'),
(N'Юхимина','w'),
(N'Яромира','w'),
(N'Романа','w'),
(N'Раїна','w'),
(N'Ходота','m'),
(N'Земислав','m'),
(N'Тонкостана','w'),
(N'Ратимир','m'),
(N'Будивид','m'),
(N'Катерина','w'),
(N'Домінік','m'),
(N'Добролик','m'),
(N'Тодора','w'),
(N'Красун','m'),
(N'Милослава','w'),
(N'Терентій','m'),
(N'Владислав','m'),
(N'П''єр','m'),
(N'Рошель','w'),
(N'Воля','w'),
(N'Славомила','w'),
(N'Віродар','m'),
(N'Євгенія','w'),
(N'Стелла','w'),
(N'Сергій','m'),
(N'Гелена','w'),
(N'Евеліна','w'),
(N'Добромила','w'),
(N'Акилина','w'),
(N'Непобор','m'),
(N'Святояр','m'),
(N'Сніжан','m'),
(N'Синьоока','w'),
(N'Марфа','w'),
(N'Велет','m'),
(N'Іван','m'),
(N'Остромов','m'),
(N'Святополк','m'),
(N'Филіцата','w'),
(N'Володар','m'),
(N'Феодосія','w'),
(N'Любомира','w'),
(N'Каленик','m'),
(N'Божко','m'),
(N'Клеопатра','w'),
(N'Артур','m'),
(N'Максим','m'),
(N'Оріяна','w'),
(N'Дана','w'),
(N'Красновида','w'),
(N'Павло','m'),
(N'Агрипина','w'),
(N'Світолика','w'),
(N'Живосил','m'),
(N'Римма','w'),
(N'Жозефіна','w')

GO

drop table if exists [surname_list_identity]
go
create table [surname_list_identity](
[id]		int			identity	Primary Key,
[surname]	nvarchar(20) not null,
[sex]		nchar(1) null,
[amount]	int not null
)
go

insert into [surname_list_identity]  ([surname], [sex], [amount])
values
('БАС',NULL,5284),
('РЕВЕНКО',NULL,5278),
('СУЛИМА','w',5278),
('ГРАБОВСЬКА','w',5271),
('БАБІЧ',NULL,5265),
('КУРИЛЕНКО',NULL,5265),
('ПЕРЕДЕРІЙ',NULL,5265),
('УДОД',NULL,5265),
('МАЩЕНКО',NULL,5258),
('ПАЛАМАР',NULL,5258),
('ІЛЬЇНА','w',5252),
('КОЗИР',NULL,5252),
('СЕРГІЙЧУК',NULL,5252),
('ТКАЧОВА','w',5245),
('БРОВКО',NULL,5239),
('БЕЖЕНАР',NULL,5232),
('КАСЯНЕНКО',NULL,5232),
('ДЕЙНЕГА','w',5226),
('КОВАЛИК',NULL,5226),
('КОЧЕРГА','w',5219),
('МАЛИШ',NULL,5219),
('МУДРИК',NULL,5219),
('МАРКЕВИЧ',NULL,5206),
('КАРАСЬ',NULL,5200),
('ХРИСТЕНКО',NULL,5200),
('ДУБОВА','w',5193),
('ЛЕБЕДЄВА','w',5193),
('ОНІЩУК',NULL,5193),
('НИКИТЮК',NULL,5187),
('ЄГОРОВ','m',5180),
('ЯКОВЛЄВ',NULL,5180),
('ГОРОШКО',NULL,5174),
('ЗЕЛЕНСЬКИЙ','m',5167),
('ФОМІН',NULL,5167),
('ЗАГОРУЛЬКО',NULL,5161),
('КАШУБА','w',5161),
('ЖУРАВЛЬОВ','m',5154),
('ПОГОРІЛИЙ','m',5154),
('ТИХОНЕНКО',NULL,5154),
('АНТОНОВ','m',5148),
('АФАНАСЬЄВА','w',5148),
('ПЛАХОТНЮК',NULL,5148),
('АНДРЮЩЕНКО',NULL,5141),
('ЖАДАН',NULL,5141),
('МАКСИМОВА','w',5128),
('ОВЧАРУК',NULL,5128),
('БОДНАРЮК',NULL,5115),
('ІВАНЕЦЬ',NULL,5115),
('МАКОГОН',NULL,5115),
('ПРЯДКО',NULL,5115),
('СТОЯНОВА','w',5109),
('ТЕСЛЯ',NULL,5109),
('КУБРАК',NULL,5102),
('ОЛЕФІР',NULL,5102),
('ОСМАНОВ','m',5102),
('БАЛЮК',NULL,5096),
('БУТКО',NULL,5096),
('ВЛАСОВ','m',5096),
('ЛІТВІНОВ','m',5096),
('ПІВТОРАК',NULL,5096),
('МУЗИЧУК',NULL,5089),
('ГЛАДЧЕНКО',NULL,5070),
('КОШЕЛЬ',NULL,5070),
('ЧАЙКОВСЬКА','w',5070),
('ЗАХАРЕНКО',NULL,5063),
('ПІДДУБНА','w',5063),
('БУГАЄНКО',NULL,5057),
('БУЛАХ',NULL,5057),
('ЛАВРЕНЧУК',NULL,5050),
('СМИК',NULL,5050),
('ДИКА','w',5044),
('КОТОВА','w',5037),
('НЕСТЕРЧУК',NULL,5031),
('БОНДАРЬ',NULL,5018),
('КАПЛУН',NULL,5018),
('КУШНІРУК',NULL,5018)
GO

/*
DECLARE @p FLOAT
DECLARE @date_birth DATE
					SET @p = RAND()

					SET @date_birth = CAST(CONCAT(CAST(CAST(@p*75 AS INT)+1926 AS CHAR(4)),'-',CAST(CAST(@p*12 AS INT)+1  AS CHAR(4)),'-',CAST(CAST(@p*30 AS INT)+1 AS CHAR(4))) AS DATE)
SELECT @date_birth

DECLARE @k INT 
DECLARE @p FLOAT
DECLARE @date_birth DATE
DECLARE @male_name INT = 100

			SET @p = RAND()
			SET @k = CAST(@p*@male_name AS INT)+1

DECLARE @q1 INT  = CAST(@k*75/(@male_name+1) AS INT)+1926
DECLARE @q2 INT  = CAST(@k*12/(@male_name+1) AS INT)+1
DECLARE @q3 INT  = CAST(@k*30/(@male_name+1) AS INT)+1
SELECT @k,@q1,@q2,@q3
					SET @date_birth = CAST(CONCAT(CAST(CAST(@k*75/(@male_name+1) AS INT)+1926 AS CHAR(4)),'-',CAST(CAST(@k*12/(@male_name+1) AS INT) + 1 AS CHAR(4)),'-',CAST(CAST(@k*30/(@male_name+1) AS INT) + 1 AS CHAR(4))) AS DATE)


go 300

DECLARE @e DATE = '2000-12-30' 
select @e*/