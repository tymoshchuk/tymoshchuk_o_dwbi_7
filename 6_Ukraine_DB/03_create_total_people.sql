﻿use [people_ua_db]
go

DROP TABLE IF EXISTS [total_people]
GO

CREATE TABLE #surname_list_male (
								[id] INT IDENTITY,
								[surname] NVARCHAR(20) NOT NULL,
								[amount] INT NOT NULL)
GO
CREATE TABLE #surname_list_female (
								[id] INT IDENTITY,
								[surname] NVARCHAR(20) NOT NULL,
								[amount] INT NOT NULL)
GO
CREATE TABLE #surname_list_hermo (
								[id] INT IDENTITY,
								[surname] NVARCHAR(20) NOT NULL,
								[amount] INT NOT NULL)
GO
CREATE TABLE #name_list_male (
								[id] INT IDENTITY,
								[name] NVARCHAR(20) NOT NULL)
GO
CREATE TABLE #name_list_female (
								[id] INT IDENTITY,
								[name] NVARCHAR(20) NOT NULL)
GO
CREATE TABLE [total people] (
								[id] INT IDENTITY,
								[surname] NVARCHAR(20) NULL,
								[name] NVARCHAR(20) NULL,
								[sex] NCHAR(1) NULL,
								[date_of_birth] DATE NULL)
GO

INSERT INTO #surname_list_male ([surname],[amount]) SELECT [surname],[amount] FROM [surname_list_identity] WHERE [sex] = 'm'
INSERT INTO #surname_list_female ([surname],[amount]) SELECT [surname],[amount] FROM [surname_list_identity] WHERE [sex] = 'w'
INSERT INTO #surname_list_hermo ([surname],[amount]) SELECT [surname],[amount] FROM [surname_list_identity] WHERE [sex] is NULL
INSERT INTO #name_list_male ([name]) SELECT [name] FROM [name_list_identity] WHERE [sex] = 'm'
INSERT INTO #name_list_female ([name]) SELECT [name] FROM [name_list_identity] WHERE [sex] = 'w'

DECLARE @hermo_surname INT = (SELECT COUNT([id]) FROM [surname_list_identity] WHERE [sex] is NULL)
DECLARE @male_surname INT = (SELECT COUNT([id]) FROM [surname_list_identity] WHERE [sex] = 'm')
DECLARE @female_surname INT = (SELECT COUNT([id]) FROM [surname_list_identity] WHERE [sex] = 'w')
DECLARE @hermo_name INT = (SELECT COUNT([id]) FROM [name_list_identity])
DECLARE @male_name INT = (SELECT COUNT([id]) FROM [name_list_identity] WHERE [sex] = 'm')
DECLARE @female_name INT = (SELECT COUNT([id]) FROM [name_list_identity] WHERE [sex] = 'w')
--SELECT @male_name,@female_name
DECLARE @i INT
DECLARE @j INT
DECLARE @k INT 
DECLARE @p FLOAT
DECLARE @date_birth DATE
DECLARE @surname NVARCHAR(20)
DECLARE @name NVARCHAR(20) 

SET @i = @male_surname
WHILE @i > 0
	BEGIN
			SET @j = (SELECT [amount] FROM #surname_list_male WHERE [id] = @i)
			SET @surname = (SELECT [surname] FROM #surname_list_male WHERE [id] = @i)
			SET @p = RAND()
			SET @k = CAST(@p*@male_name AS INT)+1
			WHILE @j > 0
				BEGIN
					SET @date_birth = CAST(CONCAT(CAST(CAST(@k*75/(@male_name+1) AS INT)+1926 AS CHAR(4)),'-',CAST(CAST(@k*12/(@male_name+1) AS INT)+1  AS CHAR(4)),'-',CAST(CAST(@k*30/(@male_name+1) AS INT)+1 AS CHAR(4))) AS DATE)
					SET @name = (SELECT [name] FROM #name_list_male WHERE [id] = @k)
					INSERT INTO [total people]([surname],[name],[sex],[date_of_birth])
					VALUES (@surname,@name,'m',@date_birth)
					SET @k = @k + 17
					IF @k > @male_name
						BEGIN
							SET @p = RAND()
							SET @k = CAST(@p*@male_name AS INT)+1
						END
					SET @j = @j - 1
				END
			SET @i = @i - 1
	END

SET @i = @female_surname
WHILE @i > 0
	BEGIN
			SET @j = (SELECT [amount] FROM #surname_list_female WHERE [id] = @i)
			SET @surname = (SELECT [surname] FROM #surname_list_female WHERE [id] = @i)
			SET @p = RAND()
			SET @k = CAST(@p*@female_name AS INT)+1
			WHILE @j > 0
				BEGIN					
					SET @date_birth = CAST(CONCAT(CAST(CAST(@k*75/(@female_name+1) AS INT)+1926 AS CHAR(4)),'-',CAST(CAST(@k*12/(@female_name+1) AS INT)+1  AS CHAR(4)),'-',CAST(CAST(@k*30/(@female_name+1) AS INT)+1 AS CHAR(4))) AS DATE)
					SET @name = (SELECT [name] FROM #name_list_female WHERE [id] = @k)
					INSERT INTO [total people]([surname],[name],[sex],[date_of_birth])
					VALUES (@surname,@name,'w',@date_birth)
					SET @k = @k + 11
					IF @k > @male_name
						BEGIN
							SET @p = RAND()
							SET @k = CAST(@p*@female_name AS INT)+1
						END
					SET @j = @j - 1
				END
			SET @i = @i - 1
	END

SET @i = @hermo_surname
WHILE @i > 0
	BEGIN
			SET @j = (SELECT [amount] FROM #surname_list_hermo WHERE [id] = @i)
			SET @surname = (SELECT [surname] FROM #surname_list_hermo WHERE [id] = @i)
			SET @p = RAND()
			SET @k = CAST(@p*@hermo_name AS INT)+1
			WHILE @j > 0
				BEGIN					
					SET @date_birth = CAST(CONCAT(CAST(CAST(@k*75/(@hermo_name+1) AS INT)+1926 AS CHAR(4)),'-',CAST(CAST(@k*12/(@hermo_name+1) AS INT)+1  AS CHAR(4)),'-',CAST(CAST(@k*30/(@hermo_name+1) AS INT)+1 AS CHAR(4))) AS DATE)
					SET @name = (SELECT [name] FROM [name_list_identity] WHERE [id] = @k)
					INSERT INTO [total people]([surname],[name],[sex],[date_of_birth])
					VALUES (@surname,@name,NULL,@date_birth)
					SET @k = @k + 23
					IF @k > @male_name
						BEGIN
							SET @p = RAND()
							SET @k = CAST(@p*@hermo_name AS INT)+1
						END
					SET @j = @j - 1
				END
			SET @i = @i - 1
	END
SELECT * FROM [total people]