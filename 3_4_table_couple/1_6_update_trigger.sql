﻿USE [T_O_module_3]
GO

DROP TRIGGER IF EXISTS [tr_update_fraction]
DROP TRIGGER IF EXISTS [tr_update_deputy]
GO

CREATE OR ALTER TRIGGER [tr_update_fraction]
ON [fraction]
AFTER UPDATE
AS
BEGIN
UPDATE [fraction]
SET [updated_date] = CURRENT_TIMESTAMP
WHERE [id] IN (SELECT [id] FROM INSERTED)
END
GO

CREATE OR ALTER TRIGGER [tr_update_deputy]
ON [deputy]
AFTER UPDATE
AS
BEGIN
UPDATE [deputy]
SET [updated_date] = CURRENT_TIMESTAMP
WHERE [id] IN (SELECT [id] FROM INSERTED)
END
GO

/*
checking in action*/
UPDATE [fraction]
SET [number_of_deputy] = 111
WHERE [id] = 1
GO

UPDATE [deputy]
SET [start_of_duties] = '2015-11-27'
WHERE [id] = 3
GO

SELECT * FROM [fraction]
SELECT * FROM [deputy]
GO