﻿USE [T_O_module_3]
GO

DROP TABLE IF EXISTS [oper].[deputy_extended]
DROP TABLE IF EXISTS [oper].[deputy_ext_oper]
GO

CREATE TABLE [oper].[deputy_extended]
(
[id] INT NOT NULL PRIMARY KEY,
[name] NVARCHAR(15) NOT NULL,
[middle_name] NVARCHAR(20) NULL,
[surname] NVARCHAR(30) NOT NULL,
[national_identification_number] BIGINT NOT NULL UNIQUE,
[date_of_birth] DATE,
[start_of_duties] DATE NOT NULL DEFAULT('2014-11-27'),
[end_of_duties] DATE NULL DEFAULT(NULL),
[fraction_id] INT NULL DEFAULT(NULL),
[electoral_district] INT NULL  DEFAULT(0),
[party_id] INT NULL DEFAULT(NULL),
[number_in_party_list] INT NULL DEFAULT(NULL),
[number_of_times_in_parlament] INT NOT NULL DEFAULT(1),
[declared_annual_incomes] MONEY NULL DEFAULT(NULL)
)
GO

CREATE TABLE [oper].[deputy_ext_oper]
(
[operation_id] INT IDENTITY(1,1) PRIMARY KEY,
[deputy_id] INT NOT NULL,
[name] NVARCHAR(15) NOT NULL,
[middle_name] NVARCHAR(20) NULL,
[surname] NVARCHAR(30) NOT NULL,
[national_identification_number] BIGINT NOT NULL,
[date_of_birth] DATE NOT NULL,
[start_of_duties] DATE NOT NULL DEFAULT('2014-11-27'),
[end_of_duties] DATE NULL DEFAULT(NULL),
[fraction_id] INT NULL DEFAULT(NULL),
[electoral_district] INT NULL DEFAULT(0),
[party_id] INT NULL DEFAULT(NULL),
[number_in_party_list] INT NULL DEFAULT(NULL),
[number_of_times_in_parlament] INT NOT NULL DEFAULT(1),
[declared_annual_incomes] MONEY NULL DEFAULT(NULL),
[operation_type] NVARCHAR(10) NULL,
[operation_date] DATETIME NULL 
)
GO

ALTER TABLE [oper].[deputy_extended]
ADD CONSTRAINT CK_deputy_extended_national_identification_number
CHECK ((10000000000>[national_identification_number])AND([national_identification_number]>=999999999))
GO

ALTER TABLE [oper].[deputy_extended]
ADD CONSTRAINT CK_deputy_extended_date_of_birth
CHECK (DATEDIFF(YEAR, [date_of_birth], '2014-11-27')>=21)
GO

ALTER TABLE [oper].[deputy_extended]
ADD CONSTRAINT CK_deputy_extended_start_of_duties
CHECK ([start_of_duties]>='2014-11-27')
GO

ALTER TABLE [oper].[deputy_extended]
ADD CONSTRAINT CK_deputy_extended_end_of_duties
CHECK ([end_of_duties]>=[start_of_duties])
GO