﻿USE [T_O_module_3]
GO

/*
we are not able to delete from fraction, as it connected to deputy as foreign key with ON DELETE NO ACTION*/

DELETE FROM [fraction]
WHERE [id] = 1
GO

/*
but we are able to delete from deputy, as it connected to fraction as foreign key with ON DELETE SET NULL*/

DELETE FROM [deputy]
WHERE [id] = 2
GO

SELECT * FROM [fraction]
SELECT * FROM [deputy]
GO

/*rollback these changes*/

INSERT INTO [deputy]
			([id],
			[name],
			[middle_name],
			[surname],
			[national_identification_number],
			[date_of_birth],
			[fraction_id]
			)
VALUES
			(2,'Артур','Володимирович','Герасимов',1123456789,'1972-08-23',1)
GO

UPDATE [fraction]
SET [head_of_fraction_id] =2,
WHERE [id] = 1
GO

SELECT * FROM [fraction]
SELECT * FROM [deputy]
GO