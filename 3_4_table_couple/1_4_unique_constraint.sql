﻿USE [T_O_module_3]
GO

/*
we are not able to make this insert, as this person has [national_identification_number] that has already been used, and it's declared to be UNIQUE*/

INSERT INTO [deputy]
			([id],
			[name],
			[middle_name],
			[surname],
			[national_identification_number],
			[date_of_birth],
			[fraction_id]
			)
VALUES
			(6,'Альфред','Альфредович','Альфреденко',4123456789,'1989-02-23',2)
GO

/*
when we changed the first digit we succeed
*/

INSERT INTO [deputy]
			([id],
			[name],
			[middle_name],
			[surname],
			[national_identification_number],
			[date_of_birth],
			[fraction_id]
			)
VALUES
			(6,'Альфред','Альфредович','Альфреденко',5123456789,'1989-02-23',2)
GO

SELECT * FROM [deputy]