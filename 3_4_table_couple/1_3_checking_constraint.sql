﻿USE [T_O_module_3]
GO

/*
we are not able to make this insert, as this person is too young to became a deputy (not allowed by constraint CK_deputy_date_of_birth 
as age is lower than 21 years)*/

INSERT INTO [deputy]
			([id],
			[name],
			[middle_name],
			[surname],
			[national_identification_number],
			[date_of_birth],
			[fraction_id]
			)
VALUES
			(5,'Альфред','Альфредович','Альфреденко',4123456789,'1999-02-23',2)
GO

/*
when we changed the date we succeed
*/

INSERT INTO [deputy]
			([id],
			[name],
			[middle_name],
			[surname],
			[national_identification_number],
			[date_of_birth],
			[fraction_id]
			)
VALUES
			(5,'Альфред','Альфредович','Альфреденко',4123456789,'1989-02-23',2)
GO

SELECT * FROM [deputy]