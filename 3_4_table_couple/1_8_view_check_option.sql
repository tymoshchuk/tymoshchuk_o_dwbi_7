﻿USE [T_O_module_3]
GO

CREATE OR ALTER VIEW [over_40_deputy_view]
	(
	[id],
	[name],
	[middle_name],
	[surname],
	[national_identification_number],
	[date_of_birth],
	[fraction_name]
	)
AS
SELECT
	d.[id],
	[name],
	[middle_name],
	[surname],
	[national_identification_number],
	[date_of_birth],
	f.[fraction_name]
FROM [deputy] d INNER JOIN [fraction] f ON d.[fraction_id] = f.[id]
WHERE YEAR(d.[date_of_birth])<=1978
WITH CHECK OPTION
GO

SELECT * FROM [over_40_deputy_view]
GO

/*we could change*/

UPDATE [over_40_deputy_view] 
SET [name] = 'Альфред'
WHERE [id] = 3
GO

SELECT * FROM [over_40_deputy_view]
GO

/*but are not allowed to change the field referring to check option*/

UPDATE [over_40_deputy_view] 
SET [date_of_birth] = '1988-03-22'
WHERE [id] = 3
GO