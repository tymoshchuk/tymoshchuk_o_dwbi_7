﻿USE [T_O_module_3]
GO

INSERT INTO [oper].[deputy_extended]
			([id],
			[name],
			[middle_name],
			[surname],
			[national_identification_number],
			[date_of_birth],
			[fraction_id]
			)
VALUES
			(1,'Тетяна','Миколаївна','Чорновол',1234567891,'1979-06-04',2),
			(2,'Артур','Володимирович','Герасимов',1123456789,'1972-08-23',1),
			(3,'Ольга','Вадимівна','Богомолець',2123456789,'1966-03-22',1),
			(4,'Максим','Юрійович','Бурбак',3123456789,'1976-01-13',2),
			(5,'Мустафа',NULL,'Джемілєв',2212345678,'1943-11-13',1)
GO

SELECT * FROM [oper].[deputy_extended]
SELECT * FROM [oper].[deputy_ext_oper]
GO

UPDATE [oper].[deputy_extended]
SET [middle_name] = 'Абдульджемільоглу'
WHERE [id] = 5
GO

SELECT * FROM [oper].[deputy_extended]
SELECT * FROM [oper].[deputy_ext_oper]
GO

DELETE FROM [oper].[deputy_extended]
WHERE [id] = 1
GO

SELECT * FROM [oper].[deputy_extended]
SELECT * FROM [oper].[deputy_ext_oper]
GO

/*conflict with check constraint, doesn't activate trigger*/

INSERT INTO [oper].[deputy_extended]
			([id],
			[name],
			[middle_name],
			[surname],
			[national_identification_number],
			[date_of_birth],
			[fraction_id]
			)
VALUES
			(6,'Альфред','Альфредович','Альфреденко',4123456789,'1999-02-23',2)
GO
