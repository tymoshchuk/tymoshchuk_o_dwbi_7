﻿USE [T_O_module_3]
GO

DROP TABLE IF EXISTS [deputy]
DROP TABLE IF EXISTS [fraction]
GO

CREATE TABLE [fraction]
(
[id] INT NOT NULL PRIMARY KEY,
[fraction_name] NVARCHAR(90) NOT NULL,
[abbreviation] NVARCHAR(8) NOT NULL,
[GUID] UNIQUEIDENTIFIER DEFAULT(NEWID()),
[head_of_fraction_id] INT NULL,
[creation_date] DATE NOT NULL DEFAULT('2014-11-27'),
[disbandment_date] DATE NULL DEFAULT(NULL),
[number_of_deputy] TINYINT NULL,
[is_member_of_coalition] BIT NOT NULL DEFAULT('true'),
[inserted_date] DATETIME NOT NULL DEFAULT(CURRENT_TIMESTAMP),
[updated_date] DATETIME NULL DEFAULT(NULL)
)
GO

CREATE TABLE [deputy]
(
[id] INT NOT NULL PRIMARY KEY,
[name] NVARCHAR(15) NOT NULL,
[middle_name] NVARCHAR(20) NULL,
[surname] NVARCHAR(30) NOT NULL,
[national_identification_number] BIGINT NOT NULL UNIQUE,
[date_of_birth] DATE,
[start_of_duties] DATE NOT NULL DEFAULT('2014-11-27'),
[end_of_duties] DATE NULL DEFAULT(NULL),
[fraction_id] INT NULL DEFAULT(NULL),
[inserted_date] DATETIME NOT NULL DEFAULT(CURRENT_TIMESTAMP),
[updated_date] DATETIME NULL DEFAULT(NULL)
)
GO


ALTER TABLE [fraction]
ADD CONSTRAINT FK_fraction_deputy
FOREIGN KEY ([head_of_fraction_id]) REFERENCES [deputy]([id])
ON DELETE SET NULL
ON UPDATE SET NULL
GO

ALTER TABLE [fraction]
ADD CONSTRAINT CK_fraction_creation_date
CHECK ([creation_date]>='2014-11-27')
GO

ALTER TABLE [fraction]
ADD CONSTRAINT CK_fraction_disbandment_date
CHECK ([disbandment_date]>=[creation_date])
GO

ALTER TABLE [fraction]
ADD CONSTRAINT CK_fraction_number_of_deputy
CHECK ([number_of_deputy]>=0)
GO

ALTER TABLE [deputy]
ADD CONSTRAINT FK_deputy_fraction
FOREIGN KEY ([fraction_id]) REFERENCES [fraction]([id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO


ALTER TABLE [deputy]
ADD CONSTRAINT FK_deputy_national_identification_number
CHECK ((10000000000>[national_identification_number])AND([national_identification_number]>=999999999))
GO

ALTER TABLE [deputy]
ADD CONSTRAINT CK_deputy_date_of_birth
CHECK (DATEDIFF(YEAR, [date_of_birth], '2014-11-27')>=21)
GO

ALTER TABLE [deputy]
ADD CONSTRAINT CK_deputy_start_of_duties
CHECK ([start_of_duties]>='2014-11-27')
GO

ALTER TABLE [deputy]
ADD CONSTRAINT CK_deputy_end_of_duties
CHECK ([end_of_duties]>=[start_of_duties])
GO

