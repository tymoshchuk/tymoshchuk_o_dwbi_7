﻿USE [T_O_module_3]
GO

INSERT INTO [fraction]
			([id],
			[fraction_name],
			[abbreviation],
			[number_of_deputy]
			)
VALUES
			(1,'Фракція партії «Блок Петра Порошенка»','БПП',136),
			(2,'Фракція політичної партії «Народний фронт»','НФ',81)
GO

INSERT INTO [deputy]
			([id],
			[name],
			[middle_name],
			[surname],
			[national_identification_number],
			[date_of_birth],
			[fraction_id]
			)
VALUES
			(1,'Тетяна','Миколаївна','Чорновол',1234567891,'1979-06-04',2),
			(2,'Артур','Володимирович','Герасимов',1123456789,'1972-08-23',1),
			(3,'Ольга','Вадимівна','Богомолець',2123456789,'1966-03-22',1),
			(4,'Максим','Юрійович','Бурбак',3123456789,'1976-01-13',2)
GO

UPDATE [fraction]
SET [head_of_fraction_id] =2,
    [updated_date] = CURRENT_TIMESTAMP
WHERE [id] = 1
GO

UPDATE [fraction]
SET [head_of_fraction_id] =4,
	[updated_date] = CURRENT_TIMESTAMP
WHERE [id] = 2
GO

SELECT * FROM [fraction]
SELECT * FROM [deputy]
GO