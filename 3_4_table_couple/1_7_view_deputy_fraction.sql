﻿USE [T_O_module_3]
GO

CREATE OR ALTER VIEW [fraction_view]
	(
	[id],
	[fraction_name],
	[abbreviation],
	[creation_date],
	[disbandment_date],
	[number_of_deputy],
	[is_member_of_coalition]
	)
AS
SELECT
	[id],
	[fraction_name],
	[abbreviation],
	[creation_date],
	[disbandment_date],
	[number_of_deputy],
	[is_member_of_coalition]
FROM [fraction]
WHERE [is_member_of_coalition] = 'true'
GO

CREATE OR ALTER VIEW [deputy_view]
	(
	[id],
	[name],
	[middle_name],
	[surname],
	[national_identification_number],
	[date_of_birth],
	[fraction_id]
	)
AS
SELECT
	[id],
	[name],
	[middle_name],
	[surname],
	[national_identification_number],
	[date_of_birth],
	[fraction_id]
FROM [deputy]
GO

SELECT * FROM [fraction_view]
SELECT * FROM [deputy_view]
GO