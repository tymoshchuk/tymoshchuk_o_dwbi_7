USE [T_O_module_3]
GO

DROP TRIGGER IF EXISTS [oper].[tr_insert_deputy_ext_oper]
DROP TRIGGER IF EXISTS [oper].[tr_delete_deputy_ext_oper]
DROP TRIGGER IF EXISTS [oper].[tr_update_deputy_ext_oper]
GO

CREATE OR ALTER TRIGGER [oper].[tr_insert_deputy_ext_oper]
ON [oper].[deputy_extended]
AFTER INSERT
AS
BEGIN
DECLARE @var_operation_type NVARCHAR(10) = 'insert'
DECLARE @var_operation_date DATETIME = CURRENT_TIMESTAMP
INSERT INTO [oper].[deputy_ext_oper]
			([deputy_id],
			[name],
			[middle_name],
			[surname],
			[national_identification_number],
			[date_of_birth],
			[start_of_duties],
			[end_of_duties],
			[fraction_id],
			[electoral_district],
			[party_id],
			[number_in_party_list],
			[number_of_times_in_parlament],
			[declared_annual_incomes],
			[operation_type],
			[operation_date]
			)
SELECT * , @var_operation_type, @var_operation_date FROM INSERTED
END
GO

CREATE OR ALTER TRIGGER [oper].[tr_delete_deputy_ext_oper]
ON [oper].[deputy_extended]
AFTER DELETE
AS
BEGIN
DECLARE @var_operation_type NVARCHAR(10) = 'delete'
DECLARE @var_operation_date DATETIME = CURRENT_TIMESTAMP
INSERT INTO [oper].[deputy_ext_oper]
			([deputy_id],
			[name],
			[middle_name],
			[surname],
			[national_identification_number],
			[date_of_birth],
			[start_of_duties],
			[end_of_duties],
			[fraction_id],
			[electoral_district],
			[party_id],
			[number_in_party_list],
			[number_of_times_in_parlament],
			[declared_annual_incomes],
			[operation_type],
			[operation_date]
			)
SELECT *, @var_operation_type, @var_operation_date FROM DELETED
END
GO

CREATE OR ALTER TRIGGER [oper].[tr_update_deputy_ext_oper]
ON [oper].[deputy_extended]
AFTER UPDATE
AS
BEGIN
DECLARE @var_operation_type NVARCHAR(10) = 'update'
DECLARE @var_operation_date DATETIME = CURRENT_TIMESTAMP
INSERT INTO [oper].[deputy_ext_oper]
			([deputy_id],
			[name],
			[middle_name],
			[surname],
			[national_identification_number],
			[date_of_birth],
			[start_of_duties],
			[end_of_duties],
			[fraction_id],
			[electoral_district],
			[party_id],
			[number_in_party_list],
			[number_of_times_in_parlament],
			[declared_annual_incomes],
			[operation_type],
			[operation_date]
			)
SELECT * , @var_operation_type, @var_operation_date FROM INSERTED
END
GO