﻿USE education
GO

DROP SYNONYM IF EXISTS [o_tymoshchuk].[fraction_edu]
DROP SYNONYM IF EXISTS [o_tymoshchuk].[deputy_edu]
DROP SYNONYM IF EXISTS [o_tymoshchuk].[deputy_extended_edu]
GO

CREATE SYNONYM [o_tymoshchuk].[fraction_edu] FOR [T_O_module_3].[dbo].[fraction]
CREATE SYNONYM [o_tymoshchuk].[deputy_edu] FOR [T_O_module_3].[dbo].[deputy]
CREATE SYNONYM [o_tymoshchuk].[deputy_extended_edu] FOR [T_O_module_3].[oper].[deputy_extended]
GO